import { Dimensions } from "react-native";

export const SITE_URL = 'https://dev.music-in.org'
export const API_URL = SITE_URL + '/api/'
export const API_BASE_URL = SITE_URL + '/'
export const API_BASE_URL_TRALING_SLASH = SITE_URL
export const PAYMENT_METHOD_LINK = SITE_URL + '/profil/paiement-method'
export const PWD_RESET_LINK = SITE_URL + '/password/reset'
// export const API_URL = 'http://192.168.1.1/api/'
// export const API_BASE_URL = 'http://192.168.1.1/'

export const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
export const APP_BORDER_RADIUS = 3;
export const MARGIN_TOP_SEPARATOR = 30;


/************************************************************************************************/
/* PLAYER */
/************************************************************************************************/

export class PlaylistItem {
    constructor(name, uri, isVideo) {
        this.name = name;
        this.uri = uri;
        this.isVideo = isVideo;
    }
}
