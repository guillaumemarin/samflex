const tintColor = '#df4137';

export default {
    tintColor,
    background: '#f1f1f1',
    tabIconDefault: '#ccc',
    tabIconSelected: tintColor,
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
    rouge: '#df4137',
    grisFonce: "#9e9e9e",
    vert: "#60d197",
    orange: "orange",
};
