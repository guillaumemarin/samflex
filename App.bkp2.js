import {useEffect, useRef} from 'react';
import {Audio} from 'expo-av';
import {AVPlaybackStatus} from 'expo-av/build/AV';

/**
 *
 * Hook reponsible of loading / unloading the sounds needed by a component
 * @param props
 */
export default function useSounds(
    sources: Array,
    playFirstOne: boolean,
) {
    const soundsRef = useRef<Audio.Sound[]>();

    useEffect(() => {
        //promise to load all sources, all at once
        Promise.all(
            sources.map((source, index) =>
                Audio.Sound.createAsync(
                    source,
                    {
                        shouldPlay: playFirstOne && index === 0,
                    },
                    handlePlaybackStatusUpdate(index),
                ),
            ),
        ).then(resources => {
           //TODO: there is a possible race condition, namely the sources change BEFORE all promises get resolved
           //before assigning the sounds to the ref, we should determine if that scenario happened
           //and instead unloadAsync the sounds to avoid memory leaks;

           soundsRef.current = resources.map(resource => resource.sound);

       }).catch(() => {
           //add the moment, if the loading fails, simply avoid playing any sounds for this component
       });

        //setup the mode for the audio session
        Audio.setAudioModeAsync({
            playsInSilentModeIOS: true,
            allowsRecordingIOS: false,
            staysActiveInBackground: true,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DUCK_OTHERS,
            shouldDuckAndroid: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DUCK_OTHERS,
            playThroughEarpieceAndroid: true,
        });

        return () => {
            if (soundsRef.current) {
                //release the memory
                soundsRef.current.map(sound => sound.unloadAsync());
                soundsRef.current = undefined;
            }
        };
    }, sources); //eslint-disable-line react-hooks/exhaustive-deps

    //
    // Fix a bug where the spotify sound level would not raise back when the sound just finished
    //
    const handlePlaybackStatusUpdate = (index) => (
        playbackStatus: AVPlaybackStatus,
    ) => {
        if (
            playbackStatus.isLoaded &&
            playbackStatus.didJustFinish &&
            soundsRef.current
        ) {
            soundsRef?.current[index].stopAsync();
        }
    };

    return soundsRef;
}
