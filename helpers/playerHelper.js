class PlaylistItem {
    constructor(name, uri, artistname, isVideo, cover, id) {
        this.name = name;
        this.uri = uri;
        this.artistname = artistname;
        this.isVideo = isVideo;
        this.cover = cover;
        this.id = id;
    }
}

export default class playerHelper{

    static buildSingleTrackPlaylist(track){
        let new_playlist = {
            "id": track.id,
            "single": false,
            "tracks": []
        }

        let add_track = new PlaylistItem(
            track.title,
            track.file,
            track.auteur,
            false,
            track.cover,
            track.id
        )
        new_playlist.tracks.push( add_track )

        return new_playlist
    }

    static buildPlaylist(album){
        let new_playlist = {
            "id": album.playlists.id,
            "single": false,
            "tracks": []
        }

        album.playlists.audios.map((track, i) => {

            let add_track = new PlaylistItem(
                track.title,
                track.file,
                album.artist.name,
                false,
                track.cover,
                track.id
            )
            new_playlist.tracks.push( add_track )
        })

        return new_playlist
    }

    static builVideoItem(video){
        let new_playlist = {
            "id": 0,
            "single": true,
            "tracks": []
        }

        let track = new PlaylistItem(
            video.title,
            video.file,
            "artist name todo",
            true,
            video.cover,
            video.id
        )

        new_playlist.tracks.push( track )
        console.log(new_playlist)

        return new_playlist
    }

}
