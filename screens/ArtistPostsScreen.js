import React from "react";

import { Image, ScrollView, Text, TouchableNativeFeedback, View, Modal, TouchableOpacity, TextInput, ActivityIndicator, RefreshControl } from "react-native";

import { LinearGradient } from 'expo-linear-gradient';

import * as RootNavigation from '../components/RootNavigation.js';
import * as Constants from '../constants/Constants.js';

import style from '../styles/st_artist'
import Api from "../api/Api";
import Loader from "../components/Loader";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import login from "../styles/login";

export default class ArtistPostsScreen extends React.Component {

    state = {
        isLoading: true,
        artist_id: null,
        artist: null,
        loggedIn: true,
        modaleSubscibeVisible: false,
        user: false,
        waitAction: false,
        bg_color: '#000'
    }

    constructor(props) {
        super(props)
    }

    componentWillUnmount() {
        console.log('unmount Artist Posts Screen')
    }

    onRefresh = async () => {
        this.setState({
            isLoading: true,
            artist: null,
        })
        await this.initArtist()
    }

    async initArtist(){
        this.setState({checkingSubscription: true})

        await this.getArtist()

        await this.props.functions.user()

        await this.subscribed()

        this.setState({checkingSubscription: false})
    }

    UNSAFE_componentWillMount = async () => {
        console.log('ArtistScreen')

        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            if( this.state.artist_id != this.props.route.params.id ){
                this.setState({
                    isLoading: true,
                    artist: null,
                })
                this.initArtist()
            }
        })

        this.initArtist()
    }

    async getArtist(){
        let artist_id = this.props.route.params.id
        this.setState({artist_id: artist_id})

        console.log('getArtist()')

        let artist = await Api.get_artist_posts('', artist_id)

        if( artist && ! artist.error ){

            console.log('getArtist() Done :)')

            this.setState({
                isLoading: false,
                artist: artist,
                bg_color: artist.bg_color
            })
        } else {
            alert(artist.error)
        }
    }

    goAlbum(album){
        RootNavigation.navigate( 'Album', { id: album.id })
    }

    goSingle(single){
        RootNavigation.navigate( 'Single', { id: single.id })
    }

    playVideo(video){
        if( this.subscribed() ){
            this.props.functions.playVideo( video )
            this.props.functions.showPlayer()
            this.props.functions.setPlayerVisible()
        } else {
            alert("Vous devez être connecté et abonné à l'artiste pour avoir accès à ce contenu")
        }
    }

    navigate(screen){
        this.props.functions.hidePlayer()
        this.props.functions.setCurrentScreen(screen)
        RootNavigation.navigate( screen )
    }

    subscribed(){

        if(this.props.route.params && this.props.route.params.subscriptions){
            console.log(this.props.route.params.subscriptions.indexOf('user_' + this.state.artist.id))
            return this.props.route.params.subscriptions.indexOf('user_' + this.state.artist.id) != -1
        }

    }

    render() {

        return (
            // <LinearGradient colors={["#000", 'transparent']} style={style.container}>
            <LinearGradient colors={[this.state.bg_color , '#000']} style={[style.container, {backgroundColor: '#000'}]}>

                {
                    this.state.waitAction == true ? (
                        <Loader title="Chargement" />
                    ) : null
                }

                {
                    this.state.isLoading === true ? (

                        <Loader title="Chargement" />

                    ) : (

                        <View style={{flex: 1}}>
                            <ScrollView style={style.scrollView}
                                        refreshControl={<RefreshControl refreshing={null} onRefresh={this.onRefresh} />}
                                        showsHorizontalScrollIndicator={false}
                                        showsVerticalScrollIndicator={false }
                            >
                                <View style={style.content}>
                                    <View style={style.header}>
                                        <Image resizeMethod="auto" resizeMode="contain" style={style.image} source={{ uri: this.state.artist.profile_thumbnail }} />
                                        <LinearGradient colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.95)']} style={style.artistNameGradient}>
                                            <Text style={style.artistName}>{ this.state.artist.name }</Text>
                                        </LinearGradient>
                                    </View>
                                </View>

                                <View style={{ marginTop: Constants.MARGIN_TOP_SEPARATOR }}></View>

                                {
                                    this.state.artist.posts.length > 0 ? (
                                        <View>
                                            <Text style={style.sectionTitle}>Posts / Actualités</Text>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.artist.posts.length > 0 ? (

                                        <View style={style.scrollView} horizontal={true} style={{ width: '100%'}}>
                                            <View style={style.postsContainer}>
                                                {
                                                    this.state.artist.posts.map((post, i) => {
                                                        return(
                                                            post.type == 'text' ? (
                                                                <View key={i} style={style.post}>
                                                                    <TouchableNativeFeedback onPress={() => this.playVideo(post.videos)} style={{height: 100}}>
                                                                        <View style={style.postContent}>
                                                                            <Text style={style.postTitle}>{ post.title }</Text>
                                                                            <Text style={style.postText}>{ post.text }</Text>
                                                                        </View>
                                                                    </TouchableNativeFeedback>
                                                                </View>
                                                            ) : post.type == 'videos' ? (
                                                                <View key={i} style={style.post}>
                                                                    <TouchableNativeFeedback onPress={() => this.playVideo(post.videos)} style={{height: 100}}>
                                                                        <View style={style.postContent}>
                                                                            <View style={[style.video, style.postVideo]}>
                                                                                <Image style={[style.albumCover, {borderBottomLeftRadius: 0, borderBottomRightRadius: 0}]} source={{ uri: post.videos.cover }} />
                                                                                <View style={style.videoPlayBtnContainer}>
                                                                                    <View style={style.videoPlayBtn}>
                                                                                        <MaterialCommunityIcons name="play" size={35} color="#FFF" />
                                                                                    </View>
                                                                                </View>
                                                                            </View>
                                                                            <Text style={style.postTitle}>{ post.title }</Text>
                                                                            <Text style={[style.postText, {marginTop: 15} ]}>{ post.text }</Text>
                                                                        </View>
                                                                    </TouchableNativeFeedback>
                                                                </View>
                                                            ) : post.type == 'photos' ? (
                                                                <View key={i} style={style.post}>
                                                                    <View>
                                                                        <View style={style.postContent}>
                                                                            <View style={[style.video, style.postImageContainer]}>
                                                                                <Image style={style.postImage} source={{ uri: post.image }} />
                                                                            </View>
                                                                            <Text style={style.postTitle}>{ post.title }</Text>
                                                                            <Text style={[style.postText, {marginTop: 15} ]}>{ post.text }</Text>
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            ) : null
                                                        )

                                                    })
                                                }
                                            </View>
                                        </View>
                                    ) : null
                                }

                                <View style={{marginTop: 60}}></View>

                            </ScrollView>
                        </View>

                    )
                }

            </LinearGradient>
        )
    }
}
