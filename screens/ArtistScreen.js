import React from "react";

import { Image, ScrollView, Text, TouchableNativeFeedback, View, Modal, TouchableOpacity, TextInput, ActivityIndicator, RefreshControl } from "react-native";

import { LinearGradient } from 'expo-linear-gradient';

import * as RootNavigation from '../components/RootNavigation.js';
import * as Constants from '../constants/Constants.js';

import style from '../styles/st_artist'
import Api from "../api/Api";
import Loader from "../components/Loader";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import login from "../styles/login";

export default class ArtistScreen extends React.Component {

    state = {
        isLoading: true,
        artist_id: null,
        artist: null,
        loggedIn: true,
        modaleSubscibeVisible: false,
        user: false,
        subscription: false,
        showSubscribe: false,
        subsciptionAmount: null,
        sendingForm: false,
        waitAction: false,
        checkingSubscription: false,
        bg_color: '#000'
    }

    constructor(props) {
        super(props)
    }

    componentWillUnmount() {
        console.log('unmount Artist Screen')
        this._unsubscribe()
        this.props.functions.stopAudioPreview()
        this.props.functions.stopPreviewTimer()
    }

    onRefresh = async () => {
        this.setState({
            isLoading: true,
            artist: null,
        })
        await this.initArtist()
    }

    async initArtist(){
        this.setState({checkingSubscription: true})

        await this.getArtist()

        await this.props.functions.user()

        await this.subscribed()

        this.setState({checkingSubscription: false})
    }

    UNSAFE_componentWillMount = async () => {
        console.log('ArtistScreen')

        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            if( this.state.artist_id != this.props.route.params.id ){
                this.setState({
                    isLoading: true,
                    artist: null,
                })
                this.initArtist()
            }
        })

        this.initArtist()
    }

    async getArtist(){
        let artist_id = this.props.route.params.id
        this.setState({artist_id: artist_id})

        console.log('getArtist()')

        let artist = await Api.get_artist('', artist_id)

        if( artist && ! artist.error ){

            console.log('getArtist() Done :)')

            this.setState({
                isLoading: false,
                artist: artist,
                bg_color: artist.bg_color
            })
        } else {
            alert(artist.error)
        }
    }

    goAlbum(album){
        RootNavigation.navigate( 'Album', { id: album.id })
    }

    goPosts(){
        RootNavigation.navigate( 'ArtistPosts', { id: this.state.artist.id })
    }

    goSingle(single){
        RootNavigation.navigate( 'Single', { id: single.id })
    }

    playVideo(video){
        if( this.subscribed() ){
            this.props.functions.playVideo( video )
            this.props.functions.showPlayer()
            this.props.functions.setPlayerVisible()
        } else {
            alert("Vous devez être connecté et abonné à l'artiste pour avoir accès à ce contenu")
        }
    }

    showLoginForm(){
        this.props.functions.showLogin()
    }

    navigate(screen){
        this.props.functions.hidePlayer()
        this.props.functions.setCurrentScreen(screen)
        RootNavigation.navigate( screen )
    }

    async showSubscribeForm(){
        this.setState({waitAction: true})

        let user = await this.props.functions.getUser()

        this.setState({waitAction: false})

        if( user ){
            this.setState({user: user})

            if( ! user.email_verified_at ){
                alert("Votre adresse email n'a pas été confirmée !\nVérifiez votre boite mail et cliquez sur le lien qui vous a été transmis :/")
                return false
            }

            if( ! user.hasPaymentMethod ){
                this.props.functions.openPaymentMethodModale()
            } else {
                this.props.functions.hideStatusBar()
                this.showSubscribeModale()
            }

        } else {

            this.showLoginForm()

        }
    }

    subscribe = async () => {
        if( ! this.state.subsciptionAmount ){
            alert('Veuilez indiquer le montant souhaité !')
            return false;
        }
        if( ! this.isInt( this.state.subsciptionAmount ) ){
            alert('Le montant indiqué n\'est pas valide !')
            return false;
        }

        this.setState({sendingForm: true})

        let dataToken = {access_token: this.state.user.token}
        let inscription = await Api.subscribe(dataToken, this.state.subsciptionAmount, this.state.artist.id)

        console.log(inscription)

        if(inscription.error){
            alert(inscription.error)
        }

        if(inscription.stripe_status && inscription.stripe_status == 'active'){
            alert('Votre abonnement a bien été pris en compte !')
            this.hideSubscribeModale()
            let user = await this.props.functions.getUser()
        }

        this.setState({sendingForm: false})

    }

    subscribed(){

        if(this.props.route.params && this.props.route.params.subscriptions){
            console.log(this.props.route.params.subscriptions.indexOf('user_' + this.state.artist.id))
            return this.props.route.params.subscriptions.indexOf('user_' + this.state.artist.id) != -1
        }

    }

    isInt(value) {
        return !isNaN(value) &&
            parseInt(Number(value)) == value &&
            !isNaN(parseInt(value, 10));
    }

    showPaymentMehtodForm(){
        this.setState({modaleSubscibeVisible: true})
    }

    hideSubscribeModale(){
        this.props.functions.showStatusBar()
        this.setState({showSubscribe: false})
    }

    showSubscribeModale(){
        this.setState({showSubscribe: true})
    }

    render() {

        return (
            <LinearGradient colors={[this.state.bg_color , '#000']} style={[style.container, {backgroundColor: '#000'}]}>

                {
                    this.state.waitAction == true ? (
                        <Loader title="Chargement" />
                    ) : null
                }

                {
                    this.state.isLoading === true ? (

                        <Loader title="Chargement" />

                    ) : (

                        <View style={{flex: 1}}>
                            <ScrollView style={style.scrollView}
                                        refreshControl={<RefreshControl refreshing={null} onRefresh={this.onRefresh} />}
                                        showsHorizontalScrollIndicator={false}
                                        showsVerticalScrollIndicator={false }
                            >

                                {
                                    /************************************************************************************************/
                                    /* HEADER */
                                    /************************************************************************************************/
                                }

                                <View style={style.content}>
                                    <View style={style.header}>
                                        <Image resizeMethod="auto" resizeMode="contain" style={style.image} source={{ uri: this.state.artist.profile_thumbnail }} />
                                        <LinearGradient colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.95)']} style={style.artistNameGradient}>
                                            <Text style={style.artistName}>{ this.state.artist.name }</Text>
                                        </LinearGradient>
                                    </View>
                                </View>

                                {
                                    /************************************************************************************************/
                                    /* BOUTON ABONNEMENT */
                                    /************************************************************************************************/
                                }

                                {
                                    this.state.checkingSubscription == true ? (
                                        <View style={[style.topActionsContainer, {width: 140, height: 80,  flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}]}>
                                            <ActivityIndicator size="small" color="#FFF"/>
                                        </View>
                                    ) : (
                                        ! this.subscribed() ? (
                                            <View style={style.topActionsContainer}>
                                                <TouchableNativeFeedback onPress={ () => this.showSubscribeForm() }>
                                                    <View style={{ backgroundColor: "#FFF", borderRadius: 3, width: 130, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15, paddingVertical: 8, justifyContent: 'center' }}>
                                                        <MaterialCommunityIcons name="account-plus-outline" size={24} color="#333" />
                                                        <Text style={{ color: '#333', marginLeft: 8, fontWeight: '700' }}>S'abonner</Text>
                                                    </View>
                                                </TouchableNativeFeedback>
                                            </View>
                                        ) : (
                                            <View style={style.topActionsContainer}>
                                                <TouchableNativeFeedback>
                                                    <View style={{ backgroundColor: "#111", borderRadius: 3, width: 120, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15, paddingVertical: 8, justifyContent: 'center' }}>
                                                        <MaterialCommunityIcons name="account-check-outline" size={24} color="#fff" />
                                                        <Text style={{ color: '#FFF', marginLeft: 8, fontWeight: '700' }}>Abonné</Text>
                                                    </View>
                                                </TouchableNativeFeedback>
                                            </View>
                                        )
                                    )
                                }

                                {
                                    /************************************************************************************************/
                                    /* UNES */
                                    /************************************************************************************************/
                                }

                                {
                                    this.state.artist.unes.length > 0 ? (
                                        <View>
                                            <View style={{ marginTop: Constants.MARGIN_TOP_SEPARATOR }}></View>
                                            <View>
                                                <Text style={style.sectionTitle}>À la une</Text>
                                            </View>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.artist.unes.length > 0 ? (
                                        <ScrollView style={style.scrollView}
                                                    horizontal={true}
                                                    style={{ width: '100%'}}
                                                    showsHorizontalScrollIndicator={false}
                                                    showsVerticalScrollIndicator={false }>
                                            <View style={style.albumContainer}>
                                                {
                                                    this.state.artist.unes.map((une, i) => {
                                                        {
                                                            if(une.type == 'album') {
                                                                return (
                                                                    <View key={i} style={style.album}>
                                                                        <TouchableNativeFeedback onPress={() => this.goAlbum( une.album )} style={{ height: 100 }}>
                                                                            <View style={{ height: '100%' }}>
                                                                                <Image style={style.albumCover} source={{ uri: une.album.cover }}/>
                                                                                <Text style={style.albumName}>{une.album.title}</Text>
                                                                                <Text style={style.date}>Album - {une.album.annee}</Text>
                                                                            </View>
                                                                        </TouchableNativeFeedback>
                                                                    </View>
                                                                )
                                                            }  else if (une.type == 'video'){
                                                                return (
                                                                    <View key={i} style={style.video}>
                                                                        <TouchableNativeFeedback onPress={() => this.playVideo(une.video)} style={{height: 100}}>
                                                                            <View style={{height: '100%'}}>
                                                                                <View>
                                                                                    <Image style={style.albumCover} source={{ uri: une.video.cover }} />
                                                                                    <View style={style.videoPlayBtnContainer}>
                                                                                        <View style={style.videoPlayBtn}>
                                                                                            <MaterialCommunityIcons name="play" size={35} color="#FFF" />
                                                                                        </View>
                                                                                    </View>
                                                                                </View>
                                                                                <Text style={style.albumName}>{ une.video.title }</Text>
                                                                                <Text style={style.date}>Vidéo</Text>
                                                                            </View>
                                                                        </TouchableNativeFeedback>
                                                                    </View>
                                                                )
                                                            } else if(une.type == 'single'){
                                                                return (
                                                                    <View key={i} style={style.album}>
                                                                        <TouchableNativeFeedback onPress={() => this.goSingle(une.single)} style={{height: 100}}>
                                                                            <View style={{height: '100%'}}>
                                                                                <Image style={style.albumCover} source={{ uri: une.single.cover }} />
                                                                                <Text style={style.albumName}>{ une.single.title }</Text>
                                                                                <Text style={style.date}>Single - { une.single.audio.annee }</Text>
                                                                            </View>
                                                                        </TouchableNativeFeedback>
                                                                    </View>
                                                                )
                                                            }
                                                        }
                                                    })
                                                }
                                            </View>
                                        </ScrollView>
                                    ) : null
                                }

                                {
                                    /************************************************************************************************/
                                    /* SINGLES */
                                    /************************************************************************************************/
                                }

                                {
                                    this.state.artist.singles.length > 0 ? (
                                        <View>
                                            <View style={{ marginTop: Constants.MARGIN_TOP_SEPARATOR }}></View>
                                            <Text style={style.sectionTitle}>Singles</Text>
                                        </View>
                                    ) : null
                                }
                                {
                                    this.state.artist.singles.length > 0 ? (
                                        <ScrollView style={style.scrollView}
                                                    horizontal={true}
                                                    style={{ width: '100%'}}
                                                    showsHorizontalScrollIndicator={false}
                                                    showsVerticalScrollIndicator={false }
                                        >
                                            <View style={style.albumContainer}>
                                                {
                                                    this.state.artist.singles.map((single, i) => {
                                                        return (
                                                            <View key={i} style={style.album}>
                                                                <TouchableNativeFeedback onPress={() => this.goSingle(single)} style={{height: 100}}>
                                                                    <View style={{height: '100%'}}>
                                                                        <Image style={style.albumCover} source={{ uri: single.cover }} />
                                                                        <Text style={style.albumName}>{ single.title }</Text>
                                                                        <Text style={style.date}>Single - { single.audio.annee }</Text>
                                                                    </View>
                                                                </TouchableNativeFeedback>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </View>
                                        </ScrollView>
                                    ) : null
                                }

                                {
                                    /************************************************************************************************/
                                    /* ALBUMS */
                                    /************************************************************************************************/
                                }

                                {
                                    this.state.artist.albums.length > 0 ? (
                                        <View>
                                            <View style={{ marginTop: Constants.MARGIN_TOP_SEPARATOR }}></View>
                                            <Text style={style.sectionTitle}>Albums / Playlistes</Text>
                                        </View>
                                    ) : null
                                }
                                {
                                    this.state.artist.albums.length > 0 ? (
                                        <ScrollView style={style.scrollView}
                                                    horizontal={true}
                                                    style={{ width: '100%'}}
                                                    showsHorizontalScrollIndicator={false}
                                                    showsVerticalScrollIndicator={false }
                                        >
                                            <View style={style.albumContainer}>
                                                {
                                                    this.state.artist.albums.map((album, i) => {
                                                        return (
                                                            <View key={i} style={style.album}>
                                                                <TouchableNativeFeedback onPress={() => this.goAlbum(album)} style={{height: 100}}>
                                                                    <View style={{height: '100%'}}>
                                                                        <Image style={style.albumCover} source={{ uri: album.cover }} />
                                                                        <Text style={style.albumName}>{ album.title }</Text>
                                                                        <Text style={style.date}>Album - { album.annee }</Text>
                                                                    </View>
                                                                </TouchableNativeFeedback>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </View>
                                        </ScrollView>
                                    ) : null
                                }

                                <View style={{ marginTop: Constants.MARGIN_TOP_SEPARATOR }}></View>

                                {
                                    /************************************************************************************************/
                                    /* VIDEOS */
                                    /************************************************************************************************/
                                }

                                {
                                    this.state.artist.videos.length > 0 ? (
                                        <View>
                                            <Text style={style.sectionTitle}>Vidéos</Text>
                                        </View>
                                    ) : null
                                }
                                {
                                    this.state.artist.videos.length > 0 ? (
                                        <ScrollView style={style.scrollView}
                                                    horizontal={true}
                                                    style={{ width: '100%'}}
                                                    showsHorizontalScrollIndicator={false}
                                                    showsVerticalScrollIndicator={false }
                                        >
                                            <View style={style.videosContainer}>
                                                {
                                                    this.state.artist.videos.map((video, i) => {
                                                        return (
                                                            <View key={i} style={style.video}>
                                                                <TouchableNativeFeedback onPress={() => this.playVideo(video)} style={{height: 100}}>
                                                                    <View style={{height: '100%'}}>
                                                                        <View>
                                                                            <Image style={style.albumCover} source={{ uri: video.cover }} />
                                                                            <View style={style.videoPlayBtnContainer}>
                                                                                <View style={style.videoPlayBtn}>
                                                                                    <MaterialCommunityIcons name="play" size={35} color="#FFF" />
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                        <Text style={style.albumName}>{ video.title }</Text>
                                                                    </View>
                                                                </TouchableNativeFeedback>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </View>
                                        </ScrollView>
                                    ) : null
                                }

                                <View style={{ marginTop: Constants.MARGIN_TOP_SEPARATOR }}></View>

                                {
                                    /************************************************************************************************/
                                    /* POSTS */
                                    /************************************************************************************************/
                                }

                                {
                                    this.state.artist.posts.length > 0 ? (
                                        <View>
                                            <Text style={style.sectionTitle}>Posts / Actualités</Text>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.artist.posts.length > 0 ? (

                                        <View style={style.scrollView} horizontal={true} style={{ width: '100%'}}>
                                            <View style={style.postsContainer}>
                                                {
                                                    this.state.artist.posts.map((post, i) => {
                                                        return(
                                                            post.type == 'text' ? (
                                                                <View key={i} style={style.post}>
                                                                    <TouchableNativeFeedback onPress={() => this.playVideo(post.videos)} style={{height: 100}}>
                                                                        <View style={style.postContent}>
                                                                            <Text style={style.postTitle}>{ post.title }</Text>
                                                                            <Text style={style.postText}>{ post.text }</Text>
                                                                        </View>
                                                                    </TouchableNativeFeedback>
                                                                </View>
                                                            ) : post.type == 'videos' ? (
                                                                <View key={i} style={style.post}>
                                                                    <TouchableNativeFeedback onPress={() => this.playVideo(post.videos)} style={{height: 100}}>
                                                                        <View style={style.postContent}>
                                                                            <View style={[style.video, style.postVideo]}>
                                                                                <Image style={[style.albumCover, {borderBottomLeftRadius: 0, borderBottomRightRadius: 0}]} source={{ uri: post.videos.cover }} />
                                                                                <View style={style.videoPlayBtnContainer}>
                                                                                    <View style={style.videoPlayBtn}>
                                                                                        <MaterialCommunityIcons name="play" size={35} color="#FFF" />
                                                                                    </View>
                                                                                </View>
                                                                            </View>
                                                                            <Text style={style.postTitle}>{ post.title }</Text>
                                                                            <Text style={[style.postText, {marginTop: 15} ]}>{ post.text }</Text>
                                                                        </View>
                                                                    </TouchableNativeFeedback>
                                                                </View>
                                                            ) : post.type == 'photos' ? (
                                                                <View key={i} style={style.post}>
                                                                    <View>
                                                                        <View style={style.postContent}>
                                                                            <View style={[style.video, style.postImageContainer]}>
                                                                                <Image style={style.postImage} source={{ uri: post.image }} />
                                                                            </View>
                                                                            <Text style={style.postTitle}>{ post.title }</Text>
                                                                            <Text style={[style.postText, {marginTop: 15} ]}>{ post.text }</Text>
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            ) : null
                                                        )

                                                    })
                                                }
                                            </View>

                                            <View style={{marginTop: 10, marginBottom: 30}}>
                                                <TouchableOpacity onPress={() => this.goPosts()}>
                                                    <Text style={{ color: "#FFF", textAlign: 'center', textDecorationLine: 'underline' }}>Voir tous les posts</Text>
                                                </TouchableOpacity>
                                            </View>

                                        </View>

                                    ) : null
                                }

                                <View style={{marginTop: 60}}></View>

                            </ScrollView>
                        </View>

                    )
                }

                {
                    /************************************************************************************************/
                    /* MODALE ABONNEMENT */
                    /************************************************************************************************/
                }
                {
                    this.state.isLoading === false ? (
                        <Modal animationType="fade" transparent={false} visible={this.state.showSubscribe} onRequestClose={() => { console.log('close') }}>
                            {
                                this.state.sendingForm == true ? (
                                    <Loader message="Veuillez patienter..." />
                                ) : null
                            }
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', width: '100%' }}>

                                <View style={login.container}>
                                    <TouchableOpacity style={login.closeBtn} onPress={ () => {this.hideSubscribeModale()} }>
                                        <MaterialCommunityIcons name="close" size={25} color="#FFF" />
                                    </TouchableOpacity>

                                    {
                                        this.state.formSuccess != null ? (
                                            <View style={{ paddingHorizontal: 15, marginBottom: 15}}>
                                                <Text style={{ color: 'white', textAlign: 'center'}}>{ this.state.formSuccess }</Text>
                                            </View>
                                        ) : null
                                    }

                                    <View style={{marginTop: 0}}>
                                        <Text style={[login.identificationViewText, {lineHeight: 30, fontWeight: '200', fontSize: 22}]}>Abonnement Artiste {"\n"}{this.state.artist.name}</Text>
                                    </View>

                                    <View style={{paddingHorizontal: 20, marginTop: 20, marginBottom: 30}}>
                                        <Text style={{color: '#fff', textAlign: 'center', lineHeight: 19}}>
                                            Veuillez indiquer le montant que vous souhaitez mettre (5€ minimum) pour vous abonner à {this.state.artist.name}.{"\n"}{"\n"}
                                            Les abonnements artistes sont souscrits pour une durée d'un an et sont renouvellés automatiquement.
                                        </Text>
                                    </View>

                                    <View style={login.textInputView}>
                                        <TextInput placeholder="Montant de l'abonnement"
                                                   value={ this.state.subsciptionAmount }
                                                   style={login.textInput}
                                                   keyboardType="numeric"
                                                   onChangeText={event => { this.setState( { subsciptionAmount: event } ) }}
                                        />
                                    </View>

                                    <TouchableOpacity onPress={() => this.subscribe()} style={[login.btnBlanc, {marginTop: 10}]}>
                                        <Text style={login.btnBlancText}>Je m'abonne</Text>
                                    </TouchableOpacity>

                                </View>

                            </View>
                        </Modal>
                    ) : null
                }

            </LinearGradient>
        )
    }
}
