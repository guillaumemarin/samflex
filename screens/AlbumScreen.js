import React from "react";

import { Text, View, Image, ScrollView, TouchableOpacity, StyleSheet, Dimensions, Modal } from "react-native";

import Animated, { Easing } from 'react-native-reanimated'

import st_album from "../styles/st_album";
import player_styles from "../styles/st_player";
import style from "../styles/st_artist";
import consts from "expo-constants";


import Api from "../api/Api";
import Loader from "../components/Loader";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import playerHelper from "../helpers/playerHelper";
import { LinearGradient } from "expo-linear-gradient";
import login from "../styles/login";
import { NavigationContainer } from "@react-navigation/native";

const {Value} = Animated
const actionsAnim = new Animated.Value(300)

export default class AlbumScreen extends React.Component {

    state = {
        isLoading: true,
        album_id: null,
        album: null,
        detailsVisible: false,
        user: null,
        clickEnabled: true,
        bg_color: '#000'
    }

    constructor(props) {
        super(props)
    }

    componentDidMount = async () => {
        console.log('AlbumScreen')
        await this.getAlbum()
        let user = this.props.functions.user()
        this.setState({user: user})
    }

    async getAlbum(){
        let album_id = this.props.route.params.id
        let token = 'plop';

        console.log('getAlbum()')

        let album = await Api.get_album(token, album_id)

        if( album && ! album.error ){

            console.log('getAlbum() done :)')

            //this.props.navigation.setOptions({ title: album.artist.name + ' - ' + album.title })

            this.setState({
                isLoading: false,
                album: album,
                isPlaying: false,
                bg_color: album.bg_color
            })

        } else {

            alert(album.error)

        }
    }

    enableClick = () => {
        this.setState({clickEnabled: true})
    }

    loadPlaylist = (from, track) => {

        // Empêche de cliquer 2 fois sur le même titre
        if( track && this.props.route.params && this.props.route.params.currentTrack == track.file ){
            return false
        }

        if( this.state.clickEnabled == true ){

            this.setState({clickEnabled: false})

            this.closeActions()

            let playlist = playerHelper.buildPlaylist( this.state.album )

            if( this.subscribed() ){
                this.props.functions.setPlaylist(playlist, from)
            } else {
                this.props.functions.audioPreview(playlist, from)
            }

            setTimeout(this.enableClick, 300)

        } else {

            console.log('Click disabled...')

        }

    }

    onpenActions(){
        Animated.timing(actionsAnim, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true,
            easing: Easing.inOut(Easing.exp)
        }).start();
    }

    closeActions(){
        Animated.timing(actionsAnim, {
            toValue: 400,
            duration: 400,
            useNativeDriver: true,
            easing: Easing.inOut(Easing.exp)
        }).start();
    }

    addToCurrentPlaylist(){
        let playlist = playerHelper.buildPlaylist(this.state.album)
        this.props.functions.addToCurrentPlaylist(playlist)
        this.closeActions()
    }

    onpenDetails(){
        this.setState({detailsVisible: true})
        this.props.functions.hideStatusBar()
        this.closeActions()
    }

    closeDetails = () => {
        this.setState({detailsVisible: false})
        this.props.functions.showStatusBar()
    }

    openAudioActions = (track) => {
        this.props.functions.openAudioActions(track)
    }

    subscribed(){
        if(this.props.route.params && this.props.route.params.subscriptions){
            return this.props.route.params.subscriptions.indexOf('user_' + this.state.album.artist.id) != -1
        }
        return false
    }

    render(){

        return (
            <View style={st_album.container}>
                { this.state.isLoading === true ? (
                    <Loader title="Chargement" />
                ) : (
                    <ScrollView style={st_album.scrollView} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false }>
                        <View style={st_album.content}>

                            <LinearGradient colors={[this.state.bg_color, '#000']} style={st_album.header}>
                                <Image style={st_album.headerImage} source={{ uri: this.state.album.cover }} />
                                <View style={st_album.headerInfos}>
                                    <Text style={st_album.albumName}>
                                        { this.state.album.title }
                                    </Text>
                                    <Text style={st_album.artistName}>
                                        { this.state.album.artist.name }
                                    </Text>
                                    <View style={st_album.description}>
                                        <Text style={st_album.short_description}>{ this.state.album.annee }</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.onpenActions()} style={st_album.headerMore}>
                                        <MaterialCommunityIcons style={st_album.actionsIcon} name="dots-vertical" size={27} color="white" />
                                    </TouchableOpacity>
                                </View>

                            </LinearGradient>

                            {
                                this.state.album.playlists && this.state.album.playlists.audios.length > 0 ? (
                                    <View style={{width: '100%', borderTopWidth: 1, borderTopColor: '#111', backgroundColor: '#00'}}>
                                        {
                                            this.state.album.playlists.audios.map((track, i) => {
                                                return (
                                                    <View key={i} style={player_styles.listItem}>
                                                        <TouchableOpacity onPress={() => this.loadPlaylist(i, track)} style={{flexDirection: 'row', alignItems: 'center'}}>
                                                            {
                                                                this.index == i ? (
                                                                    this.state.isPlaying ? (
                                                                        <MaterialCommunityIcons name="pause" size={24} color="white" />
                                                                    ) : (
                                                                        <MaterialCommunityIcons name="play" size={24} color="white" />
                                                                    )
                                                                ) : null
                                                            }
                                                            <View style={player_styles.listItemNum}>
                                                                <Text style={{fontWeight: 'bold', color: this.props.route.params && this.props.route.params.currentTrack != track.file ? '#FFF' : '#666'}}>{i+1}</Text>
                                                            </View>
                                                            <View style={player_styles.listItemInfos}>
                                                                <Text style={{ fontWeight: 'bold', lineHeight: 18, marginLeft: 7, color: this.props.route.params && this.props.route.params.currentTrack != track.file ? '#FFF' : '#666' }}>
                                                                    {track.title}
                                                                </Text>
                                                                <Text style={{ lineHeight: 18, marginLeft: 7, fontSize: 11, fontStyle: 'italic', color: this.props.route.params && this.props.route.params.currentTrack != track.file ? '#FFF' : '#555' }}>
                                                                    {this.state.album.artist.name}
                                                                </Text>
                                                            </View>
                                                            <TouchableOpacity onPress={() => this.openAudioActions(track)} style={player_styles.listItemToggle}>
                                                                <MaterialCommunityIcons style={st_album.actionsIcon} name="dots-vertical" size={22} color="white" />
                                                            </TouchableOpacity>

                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                ) : null
                            }
                        </View>

                        <View style={{marginTop: 30}}></View>

                        {/*MODAL ALBUM*/}
                        <Modal animationType="fade" transparent={true} visible={this.state.detailsVisible} onRequestClose={() => { console.log('ok') }}>
                            <View style={st_album.detailsContainer}>
                                <TouchableOpacity style={st_album.details_closeBtn} onPress={ () => {this.closeDetails()} }>
                                    <MaterialCommunityIcons name="close" size={28} color="#fff" />
                                </TouchableOpacity>
                                <View style={st_album.details_header_container}>
                                    <Image style={st_album.details_headerImage} source={{ uri: this.state.album.cover }} />
                                </View>
                                <ScrollView style={st_album.details_scrollview} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false }>

                                    <View style={st_album.details_headerInfos}>

                                        <View style={st_album.details_header}>
                                            <LinearGradient colors={['rgba(0,0,0,.05)', 'rgba(0,0,0,.9)']} style={st_album.details_artistNameGradient}>
                                                <Text style={st_album.details_albumName}>{ this.state.album.title }</Text>
                                                <Text style={st_album.details_artistName}>{ this.state.album.artist.name }</Text>
                                            </LinearGradient>
                                        </View>

                                        {
                                            this.state.album.annee ? (
                                                <View style={st_album.details_info}>
                                                    <Text style={st_album.details_info_label}>Année</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.album.annee }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.album.auteur ? (
                                                <View style={st_album.details_info}>
                                                    <Text style={st_album.details_info_label}>Compositeur / Auteur</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.album.auteur }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.album.producteur ? (
                                                <View style={st_album.details_info}>
                                                    <Text style={st_album.details_info_label}>Producteur</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.album.producteur }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.album.editeur ? (
                                                <View style={st_album.details_info}>
                                                    <Text style={st_album.details_info_label}>Éditeur</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.album.editeur }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.album.source ? (
                                                <View style={st_album.details_info}>
                                                    <Text style={st_album.details_info_label}>Source</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.album.source }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.album.lineup ? (
                                                <View style={st_album.details_info}>
                                                    <Text style={st_album.details_info_label}>Interprètes / Line up</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.album.lineup }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.album.notes ? (
                                                <View style={st_album.details_info}>
                                                    <Text style={st_album.details_info_label}>Notes</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.album.notes }</Text>
                                                </View>
                                            ) : null
                                        }

                                    </View>
                                </ScrollView>
                            </View>
                        </Modal>

                    </ScrollView>

                ) }

                {/*MENU ALBUM*/}
                <Animated.View style={[st_album.actionsContainer, animsStyles.actionsContainerMinimized ]}>
                    <View style={st_album.actionsHeader}>
                        <TouchableOpacity onPress={() => this.closeActions()} style={st_album.actionsClose}>
                            <MaterialCommunityIcons name="chevron-down" size={24} color="white" />
                        </TouchableOpacity>
                    </View>
                    <View style={st_album.actionsLinks}>
                        <TouchableOpacity onPress={() => this.onpenDetails()} style={st_album.actionsLink}>
                            <MaterialCommunityIcons style={st_album.actionsIcon} name="information-outline" size={24} color="white" />
                            <Text style={st_album.actionsLabel}>Informations sur l'album</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.subscribed() ? this.loadPlaylist(0, null) : alert("Vous devez être connecté et abonné à l'artiste pour avoir accès à cette fonctionnalité")} style={st_album.actionsLink}>
                            <MaterialCommunityIcons style={st_album.actionsIcon} name="playlist-play" size={24} color="white" />
                            <Text style={st_album.actionsLabel}>Tout lire</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.subscribed() ? this.addToCurrentPlaylist() : alert("Vous devez être connecté et abonné à l'artiste pour avoir accès à cette fonctionnalité")} style={st_album.actionsLink}>
                            <MaterialCommunityIcons style={st_album.actionsIcon} name="playlist-plus" size={24} color="white" />
                            <Text style={st_album.actionsLabel}>Lire à la suite</Text>
                        </TouchableOpacity>
                    </View>
                </Animated.View>

            </View>
        )
    }
}

const animsStyles = StyleSheet.create( {
    actionsContainerMinimized: {
        transform: [
            { translateY: actionsAnim },
        ],
    },
    detailsContainerMinimized: {
        transform: [
            { translateY: actionsAnim },
        ],
    },
})
