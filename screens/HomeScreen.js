import React from "react";

import { Image, ScrollView, Text, TouchableNativeFeedback, View, Modal, RefreshControl } from "react-native";

import { WebView } from 'react-native-webview';

import * as Constants from "../constants/Constants"
import { LinearGradient } from 'expo-linear-gradient';

import * as RootNavigation from '../components/RootNavigation.js';

import style from '../styles/home'
import Api from "../api/Api";
import Loader from "../components/Loader";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import st_artistes from "../styles/st_artists";
import * as ScreenOrientation from "expo-screen-orientation";

export default class HomeScreen extends React.Component {

    state = {
        isLoading: true,
        artist_id: null,
        artists: null,
        loggedIn: false,
        modaleSubscibeVisible: false,
        user: null
    }

    constructor(props) {
        super(props)
    }

    playVideo(video){
        this.props.functions.playVideo( video )
        this.props.functions.showPlayer()
        this.props.functions.setPlayerVisible()
    }

    showLoginForm(){
        this.props.functions.showLogin()
    }

    async getArtists(){
        let token = '';
        console.log('getArtists()')
        let artists = await Api.get_artists(token)

        if( artists && ! artists.error ){
            console.log('getArtists() Done :)')
            this.setState({
                isLoading: false,
                artists: artists
            })
        } else {
            alert(artists.error)
        }
    }

    componentDidMount = async () => {
        console.log('HomScreen')

        await this.getArtists()

        let user = this.props.functions.user()
        this.setState({user: user})
        if(user){
            this.setState({user: true})
        }
        this.setState({isLoading: false})
    }

    onRefresh = async () => {
        await this.getArtists()

        this.setState({isLoading: true})

        let user = this.props.functions.user()
        this.setState({user: user})
        if(user){
            this.setState({user: true})
        }
        this.setState({isLoading: false})
    }

    goArtist(id){
        RootNavigation.navigate( 'Artist', { id: id })
    }

    navigate(screen){
        this.props.functions.hidePlayer()
        this.props.functions.setCurrentScreen(screen)
        RootNavigation.navigate( screen )
    }

    didFocusSubscription = this.props.navigation.addListener(
        'focus',
        payload => async () => {
            this.props.functions.resetPreviewTimer()
            await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP)
        }
    )

    render() {

        return (
            //<LinearGradient colors={["#000", 'transparent']} style={[style.container]}>
            <LinearGradient colors={["rgba(79, 68, 141, .2)", 'transparent']} style={[style.container]}>
                {
                    this.state.isLoading === true ? (

                        <Loader title="Chargement" />

                    ) : (

                        <View style={{flex: 1}}>
                            <ScrollView style={style.scrollView}
                                        showsHorizontalScrollIndicator={false}
                                        showsVerticalScrollIndicator={false }
                                        refreshControl={<RefreshControl
                                            refreshing={null} onRefresh={this.onRefresh} />}
                            >
                                <View style={style.content}>
                                    <View style={{ alignItems: 'center', paddingBottom: 0}}>
                                        <Image style={{width: 290, height: 130}} resizeMode="contain" source={require('../assets/logo4.png')} />
                                    </View>
                                    <View style={{ alignItems: 'center', paddingBottom: 20}}>
                                        <Text style={{color: '#DDD'}}>L'appli qu'elle est bien pour les artistes :)</Text>
                                    </View>
                                </View>

                                <View style={[style.topActionsContainer, {justifyContent: 'center'}]}>
                                    {
                                        this.props.route.params && this.props.route.params.loggedIn == false ? (
                                            <TouchableNativeFeedback onPress={ () => this.showLoginForm() }>
                                                <View style={{ backgroundColor: "#FFF", borderRadius: 3, width: 140, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 12, paddingVertical: 10, justifyContent: 'center' }}>
                                                    <MaterialCommunityIcons name="login" size={15} color="black" />
                                                    <Text style={{ color: '#333', marginLeft: 6, fontWeight: '700' }}>Me connecter</Text>
                                                </View>
                                            </TouchableNativeFeedback>
                                        ) : null
                                    }
                                </View>

                                {
                                    this.state.artists.length > 0 ? (
                                        <View>
                                            <Text style={[style.sectionTitle, {marginBottom: 10}]}>Artistes</Text>
                                        </View>
                                    ) : null
                                }
                                <View style={{paddingHorizontal: 10, justifyContent: 'space-between', flexDirection: 'row', flex: 1, flexWrap: 'wrap'}}>
                                    { this.state.isLoading === false ? (
                                        this.state.artists.map((artist, i) => {
                                            return (
                                                <View key={i} style={st_artistes.artist}>
                                                    <TouchableNativeFeedback onPress={() => this.goArtist(artist.id)} style={{height: '100%'}}>
                                                        <View style={{height: '100%'}}>
                                                            <Image style={st_artistes.image} source={{ uri: artist.profile_thumbnail }} />
                                                            <Text style={st_artistes.artistName}>{ artist.name }</Text>
                                                        </View>
                                                    </TouchableNativeFeedback>
                                                </View>
                                            )
                                        })
                                    ) : null }
                                </View>

                                <View style={{marginTop: 20}}></View>

                            </ScrollView>
                        </View>

                    )
                }

            </LinearGradient>
        )
    }
}
