import React from "react";

import { Text, View, Image, ScrollView, TouchableOpacity, StyleSheet } from "react-native";

import Animated, { Easing } from 'react-native-reanimated'

import st_album from "../styles/st_album";

import Api from "../api/Api";
import Loader from "../components/Loader";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import playerHelper from "../helpers/playerHelper";
import { LinearGradient } from "expo-linear-gradient";

const {Value} = Animated
const actionsAnim = new Animated.Value(300)

export default class SingleScreen extends React.Component {

    state = {
        isLoading: true,
        single_id: null,
        single: null,
        detailsVisible: false,
        user: null,
        clickEnabled: true,
        bg_color: '#000',
        playing: false
    }

    constructor(props) {
        super(props)
    }

    componentDidMount = async () => {
        console.log('AlbumScreen')
        await this.getSingle()
        let user = this.props.functions.user()
        this.setState({user: user})
    }

    async getSingle(){
        let single_id = this.props.route.params.id
        let token = 'plop';

        console.log('getSingle()')

        let single = await Api.get_single(token, single_id)

        if( single && ! single.error ){

            console.log('getSingle() done :)')

            this.setState({
                isLoading: false,
                single: single,
                isPlaying: false,
                bg_color: single.bg_color
            })

        } else {

            alert(single.error)

        }
    }

    enableClick = () => {
        this.setState({clickEnabled: true})
    }

    onpenActions(){
        Animated.timing(actionsAnim, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true,
            easing: Easing.inOut(Easing.exp)
        }).start();
    }

    closeActions(){
        Animated.timing(actionsAnim, {
            toValue: 400,
            duration: 400,
            useNativeDriver: true,
            easing: Easing.inOut(Easing.exp)
        }).start();
    }

    addToCurrentPlaylist(){
        let playlist = playerHelper.buildSingleTrackPlaylist(this.state.single.audio)
        this.props.functions.addToCurrentPlaylist(playlist)
        this.closeActions()
    }

    isCurrentTrack = () => {
        let track = this.state.single.audio

        if( this.props.route.params && this.props.route.params.currentTrack == track.file ){
            return true
        }
    }

    pause = () => {
        if( this.subscribed() ){
            this.props.functions.pauseTrack()
            this.setState({playing: false})
        } else {
            this.props.functions.stopAudioPreview()
            this.setState({playing: false})
        }
    }

    resume = () => {
        if( this.subscribed() ){
            this.props.functions.pauseTrack()
            this.setState({playing: true})
        } else {
            this.loadPlaylist(0)
        }
    }

    loadPlaylist = (from) => {
        let track = this.state.single.audio

        // Empêche de cliquer 2 fois sur le même titre
        if( this.props.route.params && this.props.route.params.currentTrack == track.file ){
            return false
        }

        if( this.state.clickEnabled == true ){
            this.setState({playing: true})

            this.setState({clickEnabled: false})

            this.closeActions()

            let playlist = playerHelper.buildSingleTrackPlaylist( this.state.single.audio )

            if( this.subscribed() ){
                this.props.functions.setPlaylist(playlist, from)
            } else {
                console.log('audioPreview')
                this.props.functions.audioPreview(playlist, from)
            }

            setTimeout(this.enableClick, 300)

        } else {

            console.log('Click disabled...')

        }

    }

    onpenDetails(){
        this.setState({detailsVisible: true})
        this.closeActions()
    }

    closeDetails = () => {
        this.setState({detailsVisible: false})
    }

    openAudioActions = (track) => {
        this.props.functions.openAudioActions(track)
    }

    subscribed(){
        if(this.props.route.params && this.props.route.params.subscriptions){
            return this.props.route.params.subscriptions.indexOf('user_' + this.state.single.artist.id) != -1
        }
        return false
    }

    renderPlayBtn = () => {
        if(this.isCurrentTrack()){
            if( this.state.playing ){
                return (
                    <TouchableOpacity onPress={() => this.pause(0)} style={st_album.headerPlay}>
                        <MaterialCommunityIcons style={st_album.headerPlayBtn} name="pause" size={25} color="#000" />
                    </TouchableOpacity>
                )
            } else {
                return (
                    <TouchableOpacity onPress={() => this.resume()} style={st_album.headerPlay}>
                        <MaterialCommunityIcons style={st_album.headerPlayBtn} name="play" size={25} color="#000" />
                    </TouchableOpacity>
                )
            }
        } else {
            return (
                <TouchableOpacity onPress={() => this.loadPlaylist(0)} style={st_album.headerPlay}>
                    <MaterialCommunityIcons style={st_album.headerPlayBtn} name="play" size={25} color="#000" />
                </TouchableOpacity>
            )
        }
    }

    render(){

        return (
            <View style={st_album.container}>
                { this.state.isLoading === true ? (
                    <Loader title="Chargement" />
                ) : (
                    <ScrollView style={st_album.scrollView} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false }>
                        <View style={st_album.content}>

                            <LinearGradient colors={["#000", '#000']}>
                                <LinearGradient colors={[this.state.bg_color, '#000']}  style={st_album.header}>
                                    <Image style={st_album.headerImage} source={{ uri: this.state.single.cover }} />
                                    <View style={st_album.headerInfos}>
                                        <Text style={st_album.albumName}>
                                            { this.state.single.title }
                                        </Text>
                                        <Text style={st_album.artistName}>
                                            { this.state.single.artist.name }
                                        </Text>
                                        <View style={st_album.description}>
                                            <Text style={st_album.short_description}>{ this.state.single.annee }</Text>
                                        </View>
                                        <TouchableOpacity onPress={() => this.onpenActions()} style={st_album.headerMore}>
                                            <MaterialCommunityIcons style={st_album.actionsIcon} name="dots-vertical" size={27} color="white" />
                                        </TouchableOpacity>
                                        {
                                            this.renderPlayBtn()
                                        }
                                    </View>
                                </LinearGradient>

                                <View style={[st_album.detailsContainer, st_album.detailsContainerNoBg]}>
                                    <View style={[st_album.details_headerInfos, {paddingTop: 10}]}>
                                        {
                                            this.state.single.audio.annee ? (
                                                <View style={[st_album.details_info, {backgroundColor: 'transparent'}]}>
                                                    <Text style={st_album.details_info_label}>Année</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.single.audio.annee }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.single.audio.auteur ? (
                                                <View style={[st_album.details_info, {backgroundColor: 'transparent'}]}>
                                                    <Text style={st_album.details_info_label}>Compositeur / Auteur</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.single.audio.auteur }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.single.audio.producteur ? (
                                                <View style={[st_album.details_info, {backgroundColor: 'transparent'}]}>
                                                    <Text style={st_album.details_info_label}>Producteur</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.single.audio.producteur }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.single.audio.editeur ? (
                                                <View style={[st_album.details_info, {backgroundColor: 'transparent'}]}>
                                                    <Text style={st_album.details_info_label}>Éditeur</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.single.audio.editeur }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.single.audio.source ? (
                                                <View style={[st_album.details_info, {backgroundColor: 'transparent'}]}>
                                                    <Text style={st_album.details_info_label}>Source</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.single.audio.source }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.single.audio.lineup ? (
                                                <View style={[st_album.details_info, {backgroundColor: 'transparent'}]}>
                                                    <Text style={st_album.details_info_label}>Interprètes / Line up</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.single.audio.lineup }</Text>
                                                </View>
                                            ) : null
                                        }

                                        {
                                            this.state.single.audio.notes ? (
                                                <View style={[st_album.details_info, {backgroundColor: 'transparent'}]}>
                                                    <Text style={st_album.details_info_label}>Notes</Text>
                                                    <Text style={st_album.details_info_content}>{ this.state.single.audio.notes }</Text>
                                                </View>
                                            ) : null
                                        }

                                    </View>
                                </View>

                            </LinearGradient>

                        </View>

                    </ScrollView>

                ) }

                {/*MENU SINGLE*/}
                <Animated.View style={[st_album.actionsContainer, animsStyles.actionsContainerMinimized ]}>
                    <View style={st_album.actionsHeader}>
                        <TouchableOpacity onPress={() => this.closeActions()} style={st_album.actionsClose}>
                            <MaterialCommunityIcons name="chevron-down" size={24} color="white" />
                        </TouchableOpacity>
                    </View>
                    <View style={st_album.actionsLinks}>
                        <TouchableOpacity onPress={() => this.loadPlaylist(0)} style={st_album.actionsLink}>
                            <MaterialCommunityIcons style={st_album.actionsIcon} name="playlist-play" size={24} color="white" />
                            <Text style={st_album.actionsLabel}>Lire</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.addToCurrentPlaylist()} style={st_album.actionsLink}>
                            <MaterialCommunityIcons style={st_album.actionsIcon} name="playlist-plus" size={24} color="white" />
                            <Text style={st_album.actionsLabel}>Lire à la suite</Text>
                        </TouchableOpacity>
                    </View>
                </Animated.View>

            </View>
        )
    }
}

const animsStyles = StyleSheet.create( {
    actionsContainerMinimized: {
        transform: [
            { translateY: actionsAnim },
        ],
    },
    detailsContainerMinimized: {
        transform: [
            { translateY: actionsAnim },
        ],
    },
})
