import React from "react";

import { Text, View, Image, TouchableNativeFeedback, RefreshControl, ScrollView } from "react-native";

import * as RootNavigation from '../components/RootNavigation.js';
import Api from '../api/Api'
import Loader from "../components/Loader";
import st_artistes from '../styles/st_artists'
import { LinearGradient } from "expo-linear-gradient";
import consts from "expo-constants";
import style from "../styles/st_artist";

export default class ArtistsScreen extends React.Component {

    state = {
        isLoading: true,
        artists: []
    }

    goArtist(id){
        console.log(id)
        RootNavigation.navigate( 'Artist', { id: id })
    }

    constructor(props) {
        super(props)
    }

    async getArtists(){
        let token = '';
        console.log('getArtists()')
        let artists = await Api.get_artists(token)

        if( artists && ! artists.error ){
            console.log('getArtists() Done :)')
            this.setState({
                isLoading: false,
                artists: artists
            })
        } else {
            alert(artists.error)
        }
    }

    componentDidMount = async () => {
        console.log('ArtistsScreen')
        await this.getArtists()
        this.setState({
            isLoading: false,
        })
    }

    onRefresh = async () => {
        this.setState({
            isLoading: true,
            artists: null,
        })
        await this.getArtists()
    }

    render(){
        return (
            <LinearGradient colors={["rgba(79, 68, 141, .2)", 'transparent']}  style={{
                flexWrap: 'wrap',
                backgroundColor: '#000',
                flexDirection: 'row' ,
                paddingHorizontal: 10,
                paddingTop: 60 + consts.statusBarHeight
            }}>

                { this.state.isLoading === true ? (
                    <Loader title="Chargement" />
                ) : null }

                <ScrollView style={st_artistes.scrollView}
                            refreshControl={<RefreshControl refreshing={false} onRefresh={this.onRefresh} />}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                >
                    <View style={{justifyContent: 'space-between', flexDirection: 'row', flex: 1, flexWrap: 'wrap'}}>
                    { this.state.isLoading === false ? (
                        this.state.artists.map((artist, i) => {
                            return (
                                <View key={i} style={st_artistes.artist}>
                                    <TouchableNativeFeedback onPress={() => this.goArtist(artist.id)} style={{height: '100%'}}>
                                        <View style={{height: '100%'}}>
                                            <Image style={st_artistes.image} source={{ uri: artist.profile_thumbnail }} />
                                            <Text numberOfLines={1} style={st_artistes.artistName}>{ artist.name }</Text>
                                        </View>
                                    </TouchableNativeFeedback>
                                </View>
                            )
                        })
                    ) : null }
                    </View>
                </ScrollView>

            </LinearGradient>
        )
    }
}
