import React from "react";
import {
    StyleSheet,
    Dimensions,
    View,
    Text,
    Modal,
    TextInput,
    TouchableOpacity,
    Platform, Image, ScrollView
} from "react-native";


import * as Constants from './constants/Constants'

import * as Linking from 'expo-linking';

import Animated, { Easing } from 'react-native-reanimated'

import { navigationRef } from './components/RootNavigation'
import * as ScreenOrientation from 'expo-screen-orientation';

import { NavigationContainer, CommonActions, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { LinearGradient } from "expo-linear-gradient";

import * as SecureStore from 'expo-secure-store';
import login from "./styles/login";
import menu from "./styles/menu";
import { StatusBar, setStatusBarHidden, setStatusBarBackgroundColor } from 'expo-status-bar';


import { enableScreens } from 'react-native-screens';
enableScreens();

import PlayerAudio from "./components/PlayerAudio";
import BottomTabs from "./components/BottomTabs";

import Api from "./api/Api";

import * as RootNavigation from "./components/RootNavigation";

// SCREENS
import HomeScreen from './screens/HomeScreen'
import ArtistsScreen from './screens/ArtistsScreen'
import ArtistScreen from './screens/ArtistScreen'
import ArtistPostsScreen from './screens/ArtistPostsScreen'
import AlbumScreen from "./screens/AlbumScreen";
import SingleScreen from "./screens/SingleScreen";

import st_album from "./styles/st_album";

import playerHelper from "./helpers/playerHelper";
import consts from "expo-constants";
import { Audio } from "expo-av";
import Loader from "./components/Loader";

const RootStack = createStackNavigator();

// PLAYER ANIMATION
const { Value }              = Animated
const SCREEN_HEIGHT          = Dimensions.get( 'window' ).height
const playerAnim             = new Animated.Value( SCREEN_HEIGHT - 115 )
const playerTranslationValue = SCREEN_HEIGHT - 115
const nav_options = {
    title: ' ',
    headerStyle: {
        backgroundColor: '#000',
    },
    headerTransparent: true,
    // cardStyle: { backgroundColor: 'transparent' },
    //header:{ style:{ position: 'absolute', backgroundColor: 'transparent', zIndex: 100, top: 0, left: 0, right: 0 } },
    animationTypeForReplace: 'push',
    headerBackTitle: ' ',
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontSize: 15,
    },
    headerBackImage: (tintColor) => (
        <View style={{ backgroundColor: 'rgba(10, 10, 30, 0.8)', borderRadius: 4, height: 40, alignContent: 'center' }}>
            <MaterialCommunityIcons name="menu-left" size={30} color="#FFF" style={{paddingHorizontal: 5, marginTop: 5}} />
        </View>
    ),
}

const NavTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        background: 'transparent',
        card: 'transparent',
        text: 'transparent',
    },
    headerStyle: {
        backgroundColor: 'transparent',
    },
    dark: true,
    cardOverlayEnabled: true,
};

const previewDuration = 60;
const statsDuration = 10;

export default class App extends React.Component {

    state = {
        showPlayer: false,
        currentScreen: "Home",
        showLogin: false,
        loginMail: "guillaumemarin@gmail.com",
        loginPassword: 'guigui',
        loginPasswordConfirm: 'guigui',
        loginPrenom: "Guillaume",
        loginNom: "Marin",
        formErrors: null,
        formSuccess: null,
        showRegister: false,
        dataGetToken: null,
        sendingForm: false,
        loggedIn: false,
        modaleAudioVisible: false,
        user: null,
        subscriptions: [],
        showPaymentMethod: false,
        sound: null,
        showPreviewPlayer: false,
        previewTimer : previewDuration,
        statsTimer: statsDuration,
        previewInterval : false,
        queue_tracks: [],
        currentPreviwTrack: null,
        showParams: false,
        currentTrack: {
            'cover': "",
            "auteur": "",
            "title": "",
            "annee": "",
        }
    }

    constructor(props) {
        super(props)
        this.playVideo               = this.playVideo.bind( this )
        this.showPlayer              = this.showPlayer.bind( this )
        this.setPlaylist             = this.setPlaylist.bind( this )
        this.setCurrentScreen        = this.setCurrentScreen.bind( this )
        this.showLogin               = this.showLogin.bind( this )
        this.isLoggedIn              = this.isLoggedIn.bind( this )
        this.setPlayerVisible        = this.setPlayerVisible.bind( this )
        this.addToCurrentPlaylist    = this.addToCurrentPlaylist.bind( this )
        this.hideStatusBar           = this.hideStatusBar.bind( this )
        this.showStatusBar           = this.showStatusBar.bind( this )
        this.openAudioActions        = this.openAudioActions.bind( this )
        this.closeAudioActions       = this.closeAudioActions.bind( this )
        this.user                    = this.user.bind( this )
        this.getUser                 = this.getUser.bind( this )
        this.openPaymentMethodModale = this.openPaymentMethodModale.bind( this )
        this.subscribed              = this.subscribed.bind( this )
        this.audioPreview            = this.audioPreview.bind( this )
        this.stopAudioPreview        = this.stopAudioPreview.bind( this )
        this.resetPreviewTimer       = this.resetPreviewTimer.bind( this )
        this.stopPreviewTimer        = this.stopPreviewTimer.bind( this )
        this.videoPreview            = this.videoPreview.bind( this )
        this.setCurrentTrack         = this.setCurrentTrack.bind( this )
        this.pauseTrack              = this.pauseTrack.bind( this )
        this.toggleParams            = this.toggleParams.bind( this )


        // Fonctions partagées avec les components
        this.sharedFunctions = {
            'hidePlayer': this.hidePlayer,
            'pauseTrack': this.pauseTrack,
            'showPlayer': this.showPlayer,
            'setPlayerVisible': this.setPlayerVisible,
            'playVideo': this.playVideo,
            'showLogin': this.showLogin,
            'setPlaylist': this.setPlaylist,
            'logout': this.logout,
            'isLoggedIn': this.isLoggedIn,
            'setCurrentScreen': this.setCurrentScreen,
            'addToCurrentPlaylist': this.addToCurrentPlaylist,
            'hideStatusBar': this.hideStatusBar,
            'showStatusBar': this.showStatusBar,
            'openAudioActions': this.openAudioActions,
            'closeAudioActions': this.closeAudioActions,
            'user': this.user,
            'getUser': this.getUser,
            'openPaymentMethodModale': this.openPaymentMethodModale,
            'subscribed': this.subscribed,
            'audioPreview': this.audioPreview,
            'stopAudioPreview': this.stopAudioPreview,
            'resetPreviewTimer': this.resetPreviewTimer,
            'stopPreviewTimer': this.stopPreviewTimer,
            'videoPreview': this.videoPreview,
            'setCurrentTrack': this.setCurrentTrack,
            'toggleParams': this.toggleParams,
        }
    }

    toggleParams = () => {
        this.setState({showParams: ! this.state.showParams})
    }

    resetPreviewTimer = () => {
        console.log('resetPreviewTimer')
        this.setState({previewTimer: previewDuration})
    }

    /************************************************************************************************/
    /* PREVIEW PLAYER */
    /************************************************************************************************/

    stopPreviewTimer = () => {
        clearInterval(this.previewInterval)
        this.previewInterval = null
    }

    startPreviewTimer = async () => {
        if( ! this.previewInterval ){
            this.previewInterval = setInterval(() => {
                this.setState({previewTimer: this.state.previewTimer - 1})
                console.log(this.state.previewTimer)
                if(this.state.previewTimer <= 0){
                    clearInterval(this.previewInterval)
                    this.previewInterval = null
                    this.stopAudioPreview()
                    this.pauseTrack()
                    this.hidePlayer()
                    alert('Preview terminée !!!')
                }
            }, 1000)
        }
    }

    async stopAudioPreview(){
        if( this.state.sound != null ){
            await this.state.sound.unloadAsync();
        }
        this.setState({showPreviewPlayer: false})
    }

    async playAudioPreview(){
        await this.startPreviewTimer()
        this.refs.player_audio._pauseAudio()
        if( this.state.sound != null ){
            await this.state.sound.playAsync()
            this.setState({showPreviewPlayer: true})
        }
    }

    _onPlaybackStatusUpdate = async status => {
        if (status.isLoaded) {
            if (status.didJustFinish && !status.isLooping) {
                await this.stopAudioPreview();
            }

            if(status.uri != this.state.currentPreviwTrack){
                let loadedTracks = this.state.queue_tracks
                if(loadedTracks.length > 1){
                    loadedTracks.map( async (track, i) => {
                        console.log(status.uri + ' => ' + track.uri)
                        if( status.uri == track.uri ){
                            await track.instance.unloadAsync()
                            console.log("delete loadedTracks " + i)
                            delete loadedTracks[i]
                        }
                    })
                }
            }
        } else {
            if (status.error) {
                console.log(`FATAL PLAYER ERROR: ${status.error}`)
            }
        }
    }

    setCurrentTrack = (uri) => {
        let base_uri = uri.replace(Constants.API_BASE_URL_TRALING_SLASH, '')
        this.setState({currentPreviwTrack: base_uri})

        navigationRef.current.dispatch({
            ...CommonActions.setParams({ currentTrack: uri })
        })
    }

    async audioPreview(playlist, from){
        let uri = playlist.tracks[from].uri

        if( this.state.currentPreviwTrack == uri ){
            return false
        }

        this.setCurrentTrack(uri)

        let source = { uri: uri }

        if( this.state.sound == null ){
            await this.setState({sound: sound})
        } else {
            await this.stopAudioPreview()
        }

        let initialStatus = {
            shouldPlay: true,
            rate: 1.0,
            shouldCorrectPitch: false,
            volume: 1.0,
            isMuted: false,
            isLooping: false,
        }

        const { sound, status } = await Audio.Sound.createAsync(
            source,
            initialStatus,
            this._onPlaybackStatusUpdate,
        )

        let loadedTracks = this.state.queue_tracks

        loadedTracks.push({"uri": source.uri.replace(Constants.API_BASE_URL_TRALING_SLASH, ''), "instance": sound})
        this.setState({sound: sound, queue_tracks: loadedTracks})

        this.playAudioPreview()

    }

    _setOrientationPortrait = (landscape) => {
        if( landscape == true ){
            //ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT);
        } else {
            //ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);
        }
    }

    async componentDidMount() {
        this.checkToken()

        //await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP);
    }

    /************************************************************************************************/
    /* STATUS BAR */
    /************************************************************************************************/

    hideStatusBar(){
        if(Platform.OS === 'ios'){
            setStatusBarHidden(true, 'none')
        }
    }

    showStatusBar(){
        if(Platform.OS === 'ios') {
            setStatusBarHidden( false, 'none' )
        }
    }

    /************************************************************************************************/
    /* AUTH */
    /************************************************************************************************/

    logout = async () => {
        console.log('logout')
        await SecureStore.deleteItemAsync('api_token')
        this.refs.bottom_tabs.setLoggedOut()
        this.setState({user: null, subscriptions: [], loggedIn: false})
        navigationRef.current.dispatch({
            ...CommonActions.setParams({ user: null, subscriptions: [], loggedIn: false }),
            source: undefined
        })
    }

    isLoggedIn(){
        return this.state.loggedIn
    }

    subscribed(artist_id){
        if( artist_id == this.state.user.id ){
            return true
        }
        let subscriptions = this.state.subscriptions;
        return subscriptions.indexOf('user_' + artist_id) != -1
    }

    hasSubscription(name){
        let subscriptions = this.state.subscriptions;
        return subscriptions.indexOf(name) != -1
    }

    async getUser(){

        console.log("getUser()")

        if( this.isLoggedIn() ){
            let user = await Api.me( this.state.dataGetToken );

            await this.setState({user: user})

            navigationRef.current.dispatch({
                ...CommonActions.setParams({ user: user }),
                source: undefined
            })

            if(user){
                user.token = this.state.dataGetToken.access_token

                console.log("Check user.subscriptions")

                let subscriptions = this.state.subscriptions;
                subscriptions.push('user_' + user.id)

                if(user.subscriptions && user.subscriptions.length){
                    user.subscriptions.map((subs, i) => {
                        if(subs.stripe_status == 'active' && ! this.hasSubscription(subs.name)){
                            subscriptions.push(subs.name)
                        }
                    })

                    await this.setState({ subscriptions: subscriptions })
                }

                navigationRef.current.dispatch({
                    ...CommonActions.setParams({ subscriptions: subscriptions }),
                    source: undefined
                })

                return user

            } else {

                await this.setState({ subscriptions: [] })

            }
        }

        return null
    }

    user(){
        if(navigationRef.current){
            navigationRef.current.dispatch({
                ...CommonActions.setParams({ user: this.state.user, subscriptions: this.state.subscriptions }),
                source: undefined
            })
        }

        return this.state.user
    }

    checkToken = async () => {
        console.log("=> checkToken()")

        // Check du token local et qu'on peut communiquer avec l'API
        let is_logged_in = await Api.check_login()

        // Si on est loggué :
        if( is_logged_in == 1 ){
            console.log('=> Logged In')

            let localToken = await SecureStore.getItemAsync('api_token')
            this.setState( { dataGetToken: JSON.parse(localToken), loggedIn: true } )

            this.getUser()

            navigationRef.current.dispatch({
                ...CommonActions.setParams({ loggedIn: true }),
                source: undefined
            })

            this.refs.bottom_tabs.setState({loggedIn: true})

        } else {
            console.log('=> Looged Out')

            navigationRef.current.dispatch({
                ...CommonActions.setParams({ loggedIn: false }),
                source: undefined
            })

            this.setState({ dataGetToken: {} })

        }
    }

    getToken = async () => {
        console.log("=> getToken()")

        this.setState( { sendingForm: true } )

        let get_token = await Api.get_token( this.state.loginMail, this.state.loginPassword );

        this.setState( { sendingForm: false } )

        if ( ! get_token.error ) {

            this.setState( { dataGetToken: get_token.data, loggedIn: true } )

            if( this.state.loginMail ){
                await SecureStore.setItemAsync( "username", this.state.loginMail )
                await SecureStore.setItemAsync( "password", this.state.loginPassword )
            }

            this.hideLogin()

            this.getUser()

            navigationRef.current.setParams({loggedIn: true})

            this.refs.bottom_tabs.login()

        } else {

            alert( get_token.error )

        }

    }

    /************************************************************************************************/
    /* PLAYER */
    /************************************************************************************************/

    setPlayerVisible = () => {
        this.setState({'showPlayer': true})
    }

    showPlayer = () => {
        let visibility = this.state.showPlayer == true ? false : true;
        this.setState({'showPlayer': visibility})

        if( visibility == true ){
            console.log('showPlayer(false)')
            Animated.timing(playerAnim, {
                toValue: 0,
                duration: 300,
                useNativeDriver: true,
                easing: Easing.inOut(Easing.exp)
            }).start();
            this.refs.player_audio.showPlayer()
            this.hideStatusBar()
        } else {
            console.log('showPlayer(true)')
            Animated.timing(playerAnim, {
                toValue: playerTranslationValue,
                duration: 300,
                useNativeDriver: true,
                easing: Easing.inOut(Easing.exp)
            }).start();
            this.refs.player_audio.hidePlayer()
            this.showStatusBar()
        }
    }

    hidePlayer = () => {
        this.refs.player_audio.hidePlayer()
        this.setState({'showPlayer': false})
        console.log('hidePlayer()')

        Animated.timing(playerAnim, {
            toValue: playerTranslationValue,
            duration: 300,
            useNativeDriver: true,
            easing: Easing.out(Easing.exp)
        }).start();
        this.showStatusBar()
    }

    setPlaylist = (playlist, from) => {
        this.refs.player_audio.setPlaylist(playlist, from)
    }

    playVideo = (video) => {
        this.refs.player_audio.playVideo(video)
    }

    videoPreview = (video) => {
        if( this.state.sound != null ) {
            this.stopAudioPreview();
        }
        this.startPreviewTimer()
        this.refs.player_audio.playVideo(video)
    }

    pauseTrack = () => {
        this.refs.player_audio._onPlayPausePressed()
    }

    setCurrentScreen = (name) => {
        this.refs.bottom_tabs.setCurrentScreen(name)
        this.setState({'currentScreen': name})
    }

    /************************************************************************************************/
    /* LOGIN */
    /************************************************************************************************/

    showLogin = () => {
        this.hideStatusBar()
        this.setState({showLogin: true})
    }

    hideLogin = () => {
        this.showStatusBar()
        this.setState({showLogin: false})
    }

    /************************************************************************************************/
    /* AUDIOS ACTIONS */
    /************************************************************************************************/

    addToCurrentPlaylist = (playlist) => {
        this.refs.player_audio.addToCurrentPlaylist(playlist)
    }

    addTrackToCurrentPlaylist(){
        console.log("modale click : addTrackToCurrentPlaylist()" )
        if( this.subscribed(this.state.currentTrack.users_id) ){
            let playlist = playerHelper.buildSingleTrackPlaylist(this.state.currentTrack)
            this.refs.player_audio.addToCurrentPlaylist(playlist)
            this.closeAudioActions()
            this.setState({showPlayer: true})
        } else {
            this.closeAudioActions()
            alert("Vous devez être connecté et abonné à l'artiste pour utiser cette fonctionnalité :)")
        }
    }

    openAudioActions(audio){
        this.setState({currentTrack: audio})
        Animated.timing(actionsAnim, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true,
            easing: Easing.inOut(Easing.exp)
        }).start();
    }

    closeAudioActions(){
        Animated.timing(actionsAnim, {
            toValue: 400,
            duration: 400,
            useNativeDriver: true,
            easing: Easing.inOut(Easing.exp)
        }).start();
    }

    onpenAudioDetails(){
        this.setState({modaleAudioVisible: true})
        this.closeAudioActions()
    }

    closeAudioDetails = () => {
        this.setState({modaleAudioVisible: false})
    }

    /************************************************************************************************/
    /* REGISTER */
    /************************************************************************************************/

    async register(){
        console.log('register')

        this.setState( { sendingForm: true } )

        let inscription = await Api.register(
            this.state.loginNom,
            this.state.loginPrenom,
            this.state.loginMail,
            this.state.loginPassword,
            this.state.loginPasswordConfirm,
        )

        this.setState( { sendingForm: false } )

        if( inscription.errors ){
            this.setState({formErrors: inscription.errors, formSuccess: null})
            console.log(inscription.errors)
        }

        if( inscription.success ){

            this.setState({
                formErrors: null,
                formSuccess: inscription.success,
                showRegister: false
            })

            await SecureStore.setItemAsync( "username", this.state.loginMail )
            await SecureStore.setItemAsync( "password", this.state.loginPassword )

            console.log(inscription.success)
        }
    }

    openPaymentMethodLink(){
        this.showStatusBar()
        this.setState({showPaymentMethod: false})
        Linking.openURL(Constants.PAYMENT_METHOD_LINK)
    }

    openPaymentMethodModale(){
        this.hideStatusBar()
        this.setState({showPaymentMethod: true})
    }

    render() {

        return (

            <View style={{ flex: 1, backgroundColor: '#000' }}>

            <NavigationContainer ref={navigationRef} theme={NavTheme}>

                <StatusBar style="light" translucent={true} hidden={false} />

                {
                    /************************************************************************************************/
                    /* NAVIGATION */
                    /************************************************************************************************/
                }

                <RootStack.Navigator detachInactiveScreens={false} mode="card" headerMode="float">
                    <RootStack.Screen ref="home" name="Home" options={nav_options}>
                        { props => <HomeScreen {...props} functions={this.sharedFunctions} /> }
                    </RootStack.Screen>
                    <RootStack.Screen ref="sortable" name="Sortable" options={nav_options}>
                        { props => <SortableScreen {...props} functions={this.sharedFunctions} /> }
                    </RootStack.Screen>
                    <RootStack.Screen ref="artists" name="Artistes" options={nav_options}>
                        { props => <ArtistsScreen {...props} functions={this.sharedFunctions} /> }
                    </RootStack.Screen>
                    <RootStack.Screen ref="artist" name="Artist" options={nav_options}>
                        { props => <ArtistScreen {...props} functions={this.sharedFunctions} /> }
                    </RootStack.Screen>
                    <RootStack.Screen ref="album" name="Album" options={nav_options}>
                        { props => <AlbumScreen {...props} functions={this.sharedFunctions} /> }
                    </RootStack.Screen>
                    <RootStack.Screen ref="single" name="Single" options={nav_options}>
                        { props => <SingleScreen {...props} functions={this.sharedFunctions} /> }
                    </RootStack.Screen>
                    <RootStack.Screen ref="artist_posts" name="ArtistPosts" options={nav_options}>
                        { props => <ArtistPostsScreen {...props} functions={this.sharedFunctions} /> }
                    </RootStack.Screen>
                </RootStack.Navigator>

                {
                    /************************************************************************************************/
                    /* PLAYER */
                    /************************************************************************************************/
                }

                <Animated.View style={[styles.player, this.state.showPlayer == true ? styles.playerVisible : styles.playerMinimized]}>
                    <PlayerAudio ref="player_audio"
                                 hidePlayer={ this.hidePlayer }
                                 setPlaylist={this.setPlaylist}
                                 showPlayer={this.showPlayer}
                                 setCurrentTrack={this.setCurrentTrack}
                                 startStatsTimer={this.startStatsTimer}
                                 stopStatsTimer={this.stopStatsTimer}
                                 _setOrientationPortrait={this._setOrientationPortrait}
                    />
                </Animated.View>

                {
                    /************************************************************************************************/
                    /* MENU BOTTOM */
                    /************************************************************************************************/
                }

                <BottomTabs ref="bottom_tabs" functions={this.sharedFunctions} currentScreen={this.state.currentScreen} />

                {
                    /************************************************************************************************/
                    /* MENU AUDIO */
                    /************************************************************************************************/
                }

                <Animated.View style={[st_album.actionsContainer, animsStyles.actionsContainerMinimized ]}>
                    <View style={st_album.actionsHeader}>
                        <TouchableOpacity onPress={() => this.closeAudioActions()} style={st_album.actionsClose}>
                            <MaterialCommunityIcons name="chevron-down" size={24} color="white" />
                        </TouchableOpacity>
                    </View>
                    <View style={st_album.actionsLinks}>
                        <TouchableOpacity onPress={() => this.onpenAudioDetails()} style={st_album.actionsLink}>
                            <MaterialCommunityIcons style={st_album.actionsIcon} name="information-outline" size={24} color="white" />
                            <Text style={st_album.actionsLabel}>Informations sur le titre</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.addTrackToCurrentPlaylist()} style={st_album.actionsLink}>
                            <MaterialCommunityIcons style={st_album.actionsIcon} name="playlist-plus" size={24} color="white" />
                            <Text style={st_album.actionsLabel}>Lire à la suite</Text>
                        </TouchableOpacity>
                    </View>
                </Animated.View>

                <View style={[styles.fakeStatusBar, {opacity: this.state.showPlayer ? 1 : 0}]}></View>

                {
                    /************************************************************************************************/
                    /* MENU PARAMETRES ETC... */
                    /************************************************************************************************/
                }

                <View style={[menu.menuContainer, {right: this.state.showParams ? 0 : (0 - Constants.DEVICE_WIDTH), opacity: this.state.showParams ? 1 : 0}]}>
                    <View style={menu.closeBtn}>
                        <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={ () => {this.toggleParams()} }>
                            <MaterialCommunityIcons name="close" size={25} color="#FFF" />
                        </TouchableOpacity>
                    </View>

                    <View style={{alignItems: 'flex-start', flexDirection: 'column'}}>
                        <TouchableOpacity style={menu.menuItem} onPress={() => { this.toggleParams() }}>
                            <Text style={{marginLeft: 8, color: "#FFF"}}>Condition générales de vente</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={menu.menuItem} onPress={() => { this.toggleParams() }}>
                            <Text style={{marginLeft: 8, color: "#FFF"}}>Conditions générales d'utilisation</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={menu.menuItem} onPress={() => { this.toggleParams() }}>
                            <Text style={{marginLeft: 8, color: "#FFF"}}>RGPD</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems: 'flex-start', flexDirection: 'column'}}>
                    {
                        this.state.loggedIn == true ? (
                            <TouchableOpacity style={menu.menuItem} onPress={() => { this.logout() }}>
                                <MaterialCommunityIcons name="logout" size={18} color="white" />
                                <Text style={{marginLeft: 8, color: "#FFF"}}>Déconnexion</Text>
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity style={menu.menuItem} onPress={() => { this.toggleParams(); this.showLogin() }}>
                                <MaterialCommunityIcons name="login" size={18} color="white" />
                                <Text style={{color: "#FFF", marginLeft: 8}}>Connexion</Text>
                            </TouchableOpacity>
                        )
                    }
                    </View>
                </View>

                <View style={[styles.previewPlayer, {opacity: this.state.showPreviewPlayer ? 1 : 0}]}>
                    <TouchableOpacity style={{ height: 40, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}} onPress={() => this.stopAudioPreview()}>
                        <Image style={{ height: 24, width: 24, marginTop: -6, marginRight: 5 }} source={require('./assets/images/equi.gif') } />
                        <MaterialCommunityIcons name="stop" size={27} color="#FFF" />
                    </TouchableOpacity>
                </View>

                {
                    /************************************************************************************************/
                    /* MODALE LOGIN */
                    /************************************************************************************************/
                }
                <Modal animationType="fade" transparent={false} visible={this.state.showLogin} onRequestClose={() => { console.log('ok') }}>
                    {
                        this.state.sendingForm == true ? (
                            <Loader message="Veuillez patienter..." />
                        ) : null
                    }
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', width: '100%' }}>

                        <View style={login.container}>
                            <TouchableOpacity style={login.closeBtn} onPress={ () => {this.hideLogin()} }>
                                <MaterialCommunityIcons name="close" size={25} color="#FFF" />
                            </TouchableOpacity>

                            {
                                this.state.formSuccess != null ? (
                                    <View style={{ paddingHorizontal: 15, marginBottom: 15}}>
                                        <Text style={{ color: 'white', textAlign: 'center'}}>{ this.state.formSuccess }</Text>
                                    </View>
                                ) : null
                            }

                            <View style={[login.title, login.titleFirst]}>
                                <Text style={login.identificationViewText}>{ this.state.showRegister == false ? 'IDENTIFICATION' : 'INSCRIPTION' }</Text>
                            </View>

                            <View style={login.card}>
                                {
                                    this.state.formErrors != null ? (
                                        <View style={{ paddingHorizontal: 15}}>
                                            <Text style={{ color: 'red'}}>{ this.state.formErrors }</Text>
                                        </View>
                                    ) : null
                                }
                                <View style={login.identificationView}>
                                    {
                                        this.state.showRegister == true ? (
                                            <View style={login.textInputView}>
                                                <TextInput placeholder="Prénom"
                                                           value={ this.state.loginPrenom }
                                                           style={login.textInput}
                                                           onChangeText={event => { this.setState( { loginPrenom: event } ) }}
                                                />
                                            </View>
                                        ) : null
                                    }
                                    {
                                        this.state.showRegister == true ? (
                                            <View style={login.textInputView}>
                                                <TextInput placeholder="Nom"
                                                           value={ this.state.loginNom }
                                                           style={login.textInput}
                                                           onChangeText={event => { this.setState( { loginNom: event } ) }}
                                                />
                                            </View>
                                        ) : null
                                    }
                                    <View style={login.textInputView}>
                                        <TextInput
                                            keyboardType="email-address"
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            placeholder="Adresse e-mail"
                                            style={login.textInput}
                                            value={ this.state.loginMail }
                                            onChangeText={event => {
                                                this.setState( { loginMail: event } );
                                            }}
                                        />
                                    </View>
                                    <View style={login.textInputView}>
                                        <TextInput placeholder="Mot de passe"
                                                   value={ this.state.loginPassword }
                                                   style={login.textInput}
                                                   secureTextEntry={true}
                                                   onChangeText={event => { this.setState( { loginPassword: event } ) }}
                                        />
                                    </View>
                                    {
                                        this.state.showRegister == false ? (
                                            <View style={{marginTop: 5}}>
                                                <TouchableOpacity onPress={() => Linking.openURL( Constants.PWD_RESET_LINK )}>
                                                    <Text style={{ color: "#CCC" }}>Mot de passe oublié ?</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ) : null
                                    }
                                    {
                                        this.state.showRegister == true ? (
                                            <View style={login.textInputView}>
                                                <TextInput placeholder="Confirmation mot de passe"
                                                           value={ this.state.loginPasswordConfirm }
                                                           style={login.textInput}
                                                           secureTextEntry={true}
                                                           onChangeText={event => { this.setState( { loginPasswordConfirm: event } ) }}
                                                />
                                            </View>
                                        ) : null
                                    }
                                </View>
                                <View style={{ flexDirection: "row", marginBottom: 20, marginTop: 20 }}>
                                    {
                                        this.state.showRegister == false ? (
                                            <TouchableOpacity onPress={() => this.getToken()} style={login.btnBlanc}>
                                                <Text style={login.btnBlancText}>Se Connecter</Text>
                                            </TouchableOpacity>
                                        ) : (
                                            <TouchableOpacity onPress={() => this.register()} style={login.btnBlanc}>
                                                <Text style={login.btnBlancText}>Je m'inscris</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                </View>
                                <View style={{ flexDirection: "column", marginBottom: 20 }}>
                                    {
                                        this.state.showRegister == false ? (
                                            <TouchableOpacity onPress={() => this.setState( { showRegister: true } )} style={{ color: "#CCC" }}>
                                                <Text style={{ color: "#CCC", textDecorationLine: 'underline' }}>Créer un compte</Text>
                                            </TouchableOpacity>
                                        ) : (
                                            <TouchableOpacity onPress={() => this.setState( { showRegister: false } )} style={{ color: "#CCC" }}>
                                                <Text style={{ color: "#CCC", textDecorationLine: 'underline' }}>Se connecter</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                </View>
                            </View>

                        </View>
                    </View>
                </Modal>

                {
                    /************************************************************************************************/
                    /* MODAL AUDIO */
                    /************************************************************************************************/
                }
                <Modal animationType="fade" transparent={true} visible={this.state.modaleAudioVisible}>
                    <View style={st_album.detailsContainer}>
                        <TouchableOpacity style={st_album.details_closeBtn} onPress={ () => {this.closeAudioDetails()} }>
                            <MaterialCommunityIcons name="close" size={28} color="#fff" />
                        </TouchableOpacity>
                        <View style={st_album.details_header_container}>
                            <Image style={st_album.details_headerImage} source={{ uri: this.state.currentTrack.cover }} />
                        </View>
                        <ScrollView style={st_album.details_scrollview} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false }>

                            <View style={st_album.details_headerInfos}>

                                <View style={st_album.details_header}>
                                    <LinearGradient colors={['rgba(0,0,0,.05)', 'rgba(0,0,0,.9)']} style={st_album.details_artistNameGradient}>
                                        <Text style={st_album.details_albumName}>{ this.state.currentTrack.title }</Text>
                                        <Text style={st_album.details_artistName}>{ this.state.currentTrack.auteur }</Text>
                                    </LinearGradient>
                                </View>

                                {
                                    this.state.currentTrack.annee ? (
                                        <View style={st_album.details_info}>
                                            <Text style={st_album.details_info_label}>Année</Text>
                                            <Text style={st_album.details_info_content}>{ this.state.currentTrack.annee }</Text>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.currentTrack.auteur ? (
                                        <View style={st_album.details_info}>
                                            <Text style={st_album.details_info_label}>Compositeur / Auteur</Text>
                                            <Text style={st_album.details_info_content}>{ this.state.currentTrack.auteur }</Text>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.currentTrack.producteur ? (
                                        <View style={st_album.details_info}>
                                            <Text style={st_album.details_info_label}>Producteur</Text>
                                            <Text style={st_album.details_info_content}>{ this.state.currentTrack.producteur }</Text>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.currentTrack.editeur ? (
                                        <View style={st_album.details_info}>
                                            <Text style={st_album.details_info_label}>Éditeur</Text>
                                            <Text style={st_album.details_info_content}>{ this.state.currentTrack.editeur }</Text>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.currentTrack.source ? (
                                        <View style={st_album.details_info}>
                                            <Text style={st_album.details_info_label}>Source</Text>
                                            <Text style={st_album.details_info_content}>{ this.state.currentTrack.source }</Text>
                                        </View>
                                    ) : null
                                }

                                {
                                    this.state.currentTrack.lineup ? (
                                        <View style={st_album.details_info}>
                                            <Text style={st_album.details_info_label}>Interprètes / Line up</Text>
                                            <Text style={st_album.details_info_content}>{ this.state.currentTrack.lineup }</Text>
                                        </View>
                                    ) : null
                                }

                            </View>
                        </ScrollView>
                    </View>
                </Modal>

                {
                    /************************************************************************************************/
                    /* MODALE PAIYMENT METHOD */
                    /************************************************************************************************/
                }
                <Modal animationType="fade" transparent={false} visible={this.state.showPaymentMethod} onRequestClose={() => { console.log('ok') }}>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', width: '100%' }}>

                        <View style={login.container}>
                            <TouchableOpacity style={login.closeBtn} onPress={ () => {this.setState({showPaymentMethod: false})} }>
                                <MaterialCommunityIcons name="close" size={25} color="#FFF" />
                            </TouchableOpacity>
                            <View style={[login.title, login.titleFirst, {paddingHorizontal: 20}]}>
                                <Text style={login.identificationViewText}>Pour continuer, vous devez ajouter un moyen de paiement.</Text>
                            </View>
                            <View style={login.card}>
                                <View style={{ flexDirection: "column", marginTop: 20 }}>
                                    <TouchableOpacity onPress={() => { this.openPaymentMethodLink() }} style={login.btnBlanc}>
                                        <Text style={{ color: "#111" }}>Accéder à mon compte</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>

            </NavigationContainer>
            </View>
        )
    }
}

// ANIM MENU BOTTOM
const actionsAnim = new Animated.Value(300)
const animsStyles = StyleSheet.create( {
    actionsContainerMinimized: {
        transform: [
            { translateY: actionsAnim },
        ],
    },
    detailsContainerMinimized: {
        transform: [
            { translateY: actionsAnim },
        ],
    },
})

const styles = StyleSheet.create({
    fakeStatusBar: {
        position: 'absolute',
        top: 0,
        left: 0,
        height: consts.statusBarHeight,
        backgroundColor: '#000',
        width: Constants.DEVICE_WIDTH,

    },
    previewPlayer: {
        position: 'absolute',
        top: 43,
        right: 15,
        height: 40,
        backgroundColor: 'rgba(10, 10, 30, 0.8)',
        width: 70,
        borderRadius: 4,
    },
    player: {
        position: "absolute",
        bottom: 0,
        height: Dimensions.get('window').height,
        zIndex: 90,
    },
    playerMinimized: {
        transform: [
            { translateY: playerAnim },
        ],
    },
    playerVisible: {
        transform: [
            { translateY: playerAnim },
        ],
    },

    // MODALE
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    openButton: {
        backgroundColor: '#F194FF',
        borderRadius: 20,
        padding: 10,
        elevation: 2,
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
});
