import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        width: '100%',
        backgroundColor: '#000'
    },
    confirmationText: {
        paddingHorizontal: 25,
        paddingBottom: 30,
        paddingTop: 25,
        color: "#fff"
    },
    hidden: {
        display: "none"
    },
    closeBtn: {
        position: 'absolute',
        top: 15,
        right: 15,
    },
    card: {
        display: "flex",
        width: "90%",
        //backgroundColor: "#222",
        borderRadius: 6,
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center",
        //borderWidth: 1,
        //borderColor: "#111",
        paddingBottom: 10,
        paddingTop: 5,
        paddingHorizontal: 5
    },
    cardTransparent: {
        width: "90%",
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center",
    },
    identificationView: {
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        paddingTop: 15
    },
    signUpView: {
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
    },
    textInput: {
        height: 40,
        borderRadius: 5,
        borderColor: "lightgray",
        backgroundColor: "#FFFFFF",
        borderWidth: 1,
        paddingLeft: 15,
        width: "100%",
    },
    textInputBig: {
        height: 45,
        borderRadius: 5,
        borderColor: "lightgray",
        backgroundColor: "#FFFFFF",
        borderWidth: 1,
        paddingLeft: 15,
        fontSize: 15,
        width: "100%",
    },
    textInputError: {
        //borderColor: 'red',
    },
    textInputView: {
        width: "90%",
        paddingBottom: 10
    },
    identificationViewText: {
        color: "#fff",
        fontSize: 20,
        width: "100%",
        textAlign: "center",
    },
    identificationViewTextProfessionnel: {
        backgroundColor: "#60d197",
        color: "#ffffff",
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 20,
        letterSpacing: 1.5,
    },
    btnText: {
        color: "#f9f9f9",
        fontSize: 15,
        textTransform: 'uppercase',
        letterSpacing: 1.2,
    },
    btnTextBig: {
        color: "#f9f9f9",
        fontSize: 17,
        letterSpacing: 1.3
    },
    btnTextNoir: {
        color: "#333",
        fontSize: 15
    },
    btnNoir: {
        backgroundColor: "#444",
        height: 40,
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 20,
        letterSpacing: 1.5,
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 10,
        display: "none"
    },
    btnBlanc: {
        backgroundColor: "#FFF",
        height: 40,
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 20,
        letterSpacing: 1.5,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0
    },
    btnBlancText: {
        color: "#000",
    },
    btnParticulier: {
        backgroundColor: "#63c2eb",
        height: 50,
        borderRadius: 5,
        paddingVertical: 15,
        paddingHorizontal: 30,
        letterSpacing: 1.5,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 30
    },
    btnProfessionel: {
        backgroundColor: "#60d197",
        height: 50,
        borderRadius: 5,
        paddingVertical: 15,
        paddingHorizontal: 30,
        letterSpacing: 1.5,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 30
    },
    btnRetour: {
        height: 50,
        borderRadius: 5,
        paddingVertical: 15,
        paddingHorizontal: 30,
        letterSpacing: 1.5,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 30,
        marginLeft: 0,
        color: "#333",
    },
    btnGris: {
        backgroundColor: "lightgray",
        height: 40,
        borderRadius: 5,
        padding: 10,
        paddingHorizontal: 20,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginLeft: 20,
    },
    title: {
        marginTop: 10
    },
    titleFirst: {
        //marginTop: -30,
        width: "100%",
    },
    textGris: {
        color: "#666",
    }
})
