import { StyleSheet } from 'react-native'
import * as Constants from '../constants/Constants'
import consts from 'expo-constants';

//const coverWidth = (Constants.DEVICE_WIDTH / 2) - 38;
const coverWidth = ((Constants.DEVICE_WIDTH / 2) - 38) > 170 ? 170 : (Constants.DEVICE_WIDTH / 2) - 38;
const ContentWIdth = Constants.DEVICE_WIDTH - 30;

export default StyleSheet.create({
    viewPager: {
        flex: 1,
        flexDirection: 'row',
    },
    page: {
        alignItems: 'flex-start',
        alignContent:'flex-start',
        alignSelf:'center',
        flexDirection: 'column',
    },
    bgGris: {
        backgroundColor: '#E8E8E8',
    },
    bgBlanc: {
        backgroundColor: '#fff',
    },
    scrollView: {
        maxHeight: Constants.DEVICE_HEIGHT - 24,
        paddingBottom: 90,
        //backgroundColor: 'pink'
    },
    videoPlayBtnContainer: {
        position: 'absolute',
        left: 0,
        top: 0,
        height: "100%",
        minWidth: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    videoPlayBtn: {
        height: 40,
        width: 40,
        backgroundColor: 'rgba(0,0,0,.2)',
        borderRadius: 100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    content: {
        //paddingBottom: 130,
    },
    topActionsContainer: {
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        overflow: 'hidden',
        //backgroundColor: '#000',
        //marginTop: 60
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: '700',
        paddingHorizontal: 15,
        //paddingTop: 23,
        paddingBottom: 5,
        color: "#fff",
    },
    image: {
        width: Constants.DEVICE_WIDTH,
        height: 300 * (Constants.DEVICE_WIDTH / 450),
        flex: 1,
        //alignSelf: 'stretch',
    },
    header: {
        flexDirection: 'column',
        //width: Constants.DEVICE_WIDTH,
        marginTop: consts.statusBarHeight,
        height: 300 * (Constants.DEVICE_WIDTH / 450),
        justifyContent: 'center',
        alignItems: 'center',
    },
    artistNameGradient: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 290,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    artistName: {
        paddingBottom: 35,
        paddingTop: 25,
        paddingHorizontal: 15,
        fontSize: 25,
        fontWeight: '700',
        lineHeight: 16,
        //backgroundColor: '#000',
        color: "#fff",
        //position: 'absolute',
        //bottom: 0,
        //left: 0,
        //width: '100%',
        //textAlign: "center"
    },
    description: {
        paddingHorizontal: 15,
        //paddingVertical: 10,
        color: "#fff",
        paddingBottom: 40,
        //minHeight: Constants.DEVICE_HEIGHT - 450
    },
    short_description: {
        fontSize: 14,
        color: "#fff",
    },
    albumContainer: {
        //width: Constants.DEVICE_WIDTH,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingTop: 15,
        //paddingBottom: 20,
        //flexWrap: 'wrap',
    },
    album: {
        //backgroundColor: '#FFF',
        height: coverWidth + 52,
        width: coverWidth,
        borderRadius: Constants.APP_BORDER_RADIUS,
        overflow: 'hidden',
        //borderWidth: 1,
        //borderColor: "#ddd",
        marginBottom: 15,
        marginRight: 15,
    },
    albumCover: {
        height: coverWidth,
        marginBottom: 7,
        borderRadius: Constants.APP_BORDER_RADIUS,
        backgroundColor: "rgba(255,255,255,.05)"
    },
    albumName: {
        //paddingHorizontal: 7
        color: "#ccc",
    },
    date: {
        fontSize: 11,
        fontStyle: 'italic',
        //paddingHorizontal: 7,
        marginBottom: 5,
        color: "#888",
    },

    videosContainer: {
        flexDirection: 'row',
        alignContent: 'flex-start',
        paddingHorizontal: 12,
        paddingTop: 12,
        //paddingBottom: 120,
    },
    video: {
        //backgroundColor: '#FFF',
        height: 200,
        width: 200,
        borderRadius: Constants.APP_BORDER_RADIUS,
        overflow: 'hidden',
        marginRight: 15,
        //borderWidth: 1,
        //borderColor: "#ddd",
        //marginBottom: 18
    },


    tabsContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'stretch',
        maxHeight: 50,
        zIndex: 10,

        shadowColor: '#000',
        shadowOffset: { width: 2, height: 1 },
        shadowOpacity: .1,
        shadowRadius: 3,
        elevation: 1,
    },
    tab: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: "relative",

    },
    tabFirst: {
        borderLeftWidth: 0,
    },
    tabActive: {
        backgroundColor: '#222'
    },
    label: {
        fontSize: 13,
        color: '#222',
    },
    labelActive: {
        color: '#FFF',
    },
    postsContainer: {
        flexDirection: 'column',
        //overflow: 'hidden',
        //flexWrap: 'wrap',
        paddingHorizontal: 15,
        paddingTop: 5
    },
    post: {
        //paddingHorizontal: 15,
        marginBottom: 15,
        marginTop: 10,
        width: Constants.DEVICE_WIDTH - 30,
        alignItems: "stretch",
    },
    postContent: {
        //paddingHorizontal: 15,
        //paddingBottom: 15,
        backgroundColor: "rgba(0,0,0,.7)",
        borderRadius: Constants.APP_BORDER_RADIUS,
    },
    postVideo: {
        width: '100%',
        height: 130,
        //paddingTop: 15,
    },
    postTitle: {
        color: '#fff',
        paddingTop: 10,
        paddingBottom: 0,
        paddingHorizontal: 15,
        fontSize: 18,
        fontWeight: '700',
    },
    postText: {
        color: '#fff',
        paddingBottom: 15,
        paddingHorizontal: 15,
        fontSize: 13,
        lineHeight: 14
    },
    postImageContainer: {
        flex: 1,
        width: ContentWIdth,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    },
    postImage: {
        width: Constants.DEVICE_WIDTH,
        resizeMode: "cover",
        height: 211,
        borderRadius: 0
    },


    // MODALE

    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    openButton: {
        backgroundColor: '#F194FF',
        borderRadius: 20,
        padding: 10,
        elevation: 2,
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
})
