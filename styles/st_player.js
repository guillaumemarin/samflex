import { Dimensions, StyleSheet } from 'react-native'

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
const BACKGROUND_COLOR = "#FFFFFF";

export default StyleSheet.create({
    emptyContainer: {
        alignSelf: "stretch",
        backgroundColor: BACKGROUND_COLOR
    },
    container: {
        flex: 1,
        flexDirection: "column",
        //alignItems: "center",
        //alignSelf: "stretch",
        height: DEVICE_HEIGHT,
        width: DEVICE_WIDTH,
        backgroundColor: BACKGROUND_COLOR,
    },
    playerHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        alignContent: 'center',
        padding: 20,
        width: "100%",
        backgroundColor: '#333',
    },
    playerHeaderTitle: {
        color: '#FFF'
    },
    minimized: {
        //bottom: 80 - DEVICE_HEIGHT
        paddingTop: 0
    },
    fullscreen: {
        paddingTop: 0
    },
    listItem: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#222',
        alignItems: "center",
    },
    listItemCover: {
        width: 40,
        height: 40,
        borderRadius: 4,
    },
    listItemNum: {
        lineHeight: 24,
        paddingVertical: 10,
        //paddingHorizontal: 20,
        width: 40,
        maxWidth: 40,
        //backgroundColor: 'red',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: "center",
        textAlign: 'center',
        color: '#FFF'
    },
    listItemInfos: {
        //alignItems: 'center',
        width: DEVICE_WIDTH - 80,
        maxWidth: DEVICE_WIDTH - 60,
        paddingVertical: 8
    },
    listItemToggle: {
        width: 35,
        opacity: 0.2,
        alignItems: 'center',
        paddingLeft: 5
    },
    wrapper: {},
    nameContainer: {
        //height: 14
    },
    space: {
        height: 14
    },
    videoContainer: {
        //height: VIDEO_CONTAINER_HEIGHT
    },
    video: {
        maxWidth: DEVICE_WIDTH
    },
    playbackContainer: {
        flex: 1,
        flexDirection: "column",
        //justifyContent: "space-between",
        alignItems: "center",
        alignSelf: "stretch",
    },
    playbackSlider: {
        alignSelf: "stretch"
    },
    timestampRow: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        alignSelf: "stretch",
        minHeight: 14
    },
    text: {
        fontSize: 14,
        minHeight: 14
    },
    buffering: {
        textAlign: "left",
        paddingLeft: 20,
        fontSize: 10,
        marginTop: -18
    },
    timestamp: {
        textAlign: "right",
        paddingRight: 20,
        fontSize: 10,
        marginTop: -18
    },
    button: {
        backgroundColor: BACKGROUND_COLOR
    },
    controlsContainer: {
        flexDirection: "column",
        height: 120,
        backgroundColor: '#F2F2F2',
        //alignItems: "center",
        justifyContent: "center",
        width: "100%",
        position: "absolute",
        bottom: 70,
        left: 0,
        paddingVertical: 23,
    },
    controlsBtn: {
        marginLeft: 10,
        backgroundColor: '#333',
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        width: 40,
        borderRadius: 30
    },
    buttonsContainerBase: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
    },
    buttonsContainerTopRow: {
        maxHeight: 51,
        minWidth: DEVICE_WIDTH / 2.0,
        maxWidth: DEVICE_WIDTH / 2.0
    },
    buttonsContainerMiddleRow: {
        maxHeight: 58,
        alignSelf: "stretch",
        paddingRight: 20
    },
    volumeContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        minWidth: DEVICE_WIDTH / 2.0,
        maxWidth: DEVICE_WIDTH / 2.0
    },
    volumeSlider: {
        width: DEVICE_WIDTH / 2.0 - 67
    },
    rateSlider: {
        width: DEVICE_WIDTH / 2.0
    },
    buttonsContainerTextRow: {
        maxHeight: 14,
        alignItems: "center",
        paddingRight: 20,
        paddingLeft: 20,
        minWidth: DEVICE_WIDTH,
        maxWidth: DEVICE_WIDTH
    }
})
