import { StyleSheet } from 'react-native'
import * as Constants from "../constants/Constants";

export default StyleSheet.create({
    menuContainer: {
        position: 'absolute',
        top: 0,
        height: Constants.DEVICE_HEIGHT,
        backgroundColor: 'rgba(0, 0, 0, 0.9)',
        width: Constants.DEVICE_WIDTH,
        paddingTop: 50,
        flex: 1
    },
    closeBtn: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
        width: Constants.DEVICE_WIDTH,
        marginBottom: 20
    },
    menuItem: {
        paddingLeft: 15,
        paddingVertical: 10,
        alignItems: 'center',
        flexDirection: 'row'
    }
})
