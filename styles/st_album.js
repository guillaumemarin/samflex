import { StyleSheet } from 'react-native'
import * as Constants from '../constants/Constants'
import consts from "expo-constants";

const CoverPadding = 80;
const CoverMargin = 20;
const CoverBackground = '#111';

export default StyleSheet.create({
    artist: {
        backgroundColor: '#FFF',
        height: 140,
        width: (Constants.DEVICE_WIDTH / 2) - 18,
        borderRadius: 5,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: "#ddd",
        marginBottom: 15
    },
    bgGris: {
        backgroundColor: '#E8E8E8',
    },
    bgBlanc: {
        backgroundColor: '#fff',
    },
    scrollView: {
        maxHeight: Constants.DEVICE_HEIGHT - 24,
        paddingBottom: 100,
    },
    content: {
        paddingBottom: 20,
        backgroundColor: 'transparent'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        //maxHeight: Constants.DEVICE_HEIGHT - 185,
        overflow: 'hidden',
        backgroundColor: '#000',
        position: 'relative',
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingHorizontal: 15,
        paddingTop: 10,
    },
    header: {
        flex: 1,
        flexDirection: 'column',
        //width: Constants.DEVICE_WIDTH,
        backgroundColor: CoverBackground,
        paddingTop: consts.statusBarHeight,
        alignItems: 'center'
    },
    headerImage: {
        height: 200,
        width: 200,
        marginVertical: CoverMargin,
        maxWidth: Constants.DEVICE_WIDTH - CoverPadding,
        maxHeight: Constants.DEVICE_WIDTH - CoverPadding,
        borderRadius: 4
    },
    headerInfos: {
        //height: Constants.DEVICE_WIDTH,
        width: Constants.DEVICE_WIDTH,
        //maxWidth: '50%',
        paddingBottom: CoverMargin,
        paddingHorizontal: 15,
    },
    headerPlay: {
        position: 'absolute',
        bottom: 10,
        left: 15,
        height: 40,
        width: 40,
        backgroundColor: 'rgba(255,255,255,.8)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 40
    },
    headerPlayBtn: {

    },
    headerMore: {
        position: 'absolute',
        bottom: 10,
        right: 0,
        height: 40,
        width: 40,
        //backgroundColor: 'rgba(255,255,255,.1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        //borderRadius: 40
        opacity: .7,
    },
    albumName: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#fff',
        textAlign: 'center'
    },
    artistName: {
        fontSize: 12,
        fontStyle: 'italic',
        lineHeight: 16,
        color: '#fff',
        textAlign: 'center'
    },
    description: {
        fontSize: 12,
        color: '#fff',
        textAlign: 'center'
    },
    short_description: {
        fontSize: 12,
        lineHeight: 19,
        color: '#fff',
        textAlign: 'center'
    },
    albumContainer: {
        //flex: 1,
        backgroundColor: '#fff',
        width: Constants.DEVICE_WIDTH,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 12,
        paddingVertical: 15
    },
    album: {
        backgroundColor: '#fff',
        height: 140,
        width: (Constants.DEVICE_WIDTH / 2) - 18,
        borderRadius: 5,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: "#ddd"
    },
    albumCover: {
        height: 100
    },
    actionsContainer: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,.90)',
        height: 350,
        width: Constants.DEVICE_WIDTH,
        left: 0,
        bottom: 0,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#111",
    },
    actionsLinks: {
        flexDirection: 'column',
        alignContent: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 5,
        paddingHorizontal: 20
    },
    actionsHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
        height: 40,
        //backgroundColor: "#111",
    },
    actionsClose: {
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'center',
    },
    actionsLink: {
        flexDirection: 'row',
        //justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    actionsLabel: {
        color: '#fff',
        fontSize: 16
    },
    actionsIcon: {
        marginRight: 5,
        fontWeight: '100'
    },

    detailsContainer: {
        backgroundColor: 'rgba(0,0,0,1)',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%'
    },
    detailsContainerNoBg: {
        backgroundColor: 'transparent'
    },
    details_closeBtn: {
        position: 'absolute',
        top: 10,
        right: 10,
        zIndex: 100,
        //backgroundColor: 'rgba(0,0,0,.2)'
    },
    details_scrollview: {
        height: Constants.DEVICE_HEIGHT,
        zIndex: 10
    },
    details_header: {
        flexDirection: 'column',
        width: Constants.DEVICE_WIDTH,
        //height: Constants.DEVICE_WIDTH,
        //marginBottom: 20,
    },
    details_header_container: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: Constants.DEVICE_WIDTH,
        height: Constants.DEVICE_WIDTH,
    },
    details_headerImage: {
        width: Constants.DEVICE_WIDTH,
        height: Constants.DEVICE_WIDTH,
    },
    details_headerInfos: {
        paddingBottom: 50,
        paddingTop: Constants.DEVICE_WIDTH,
    },
    details_artistNameGradient: {
        position: 'absolute',
        width: Constants.DEVICE_WIDTH,
        left: 0,
        bottom: 0,
        //height: 300,
        height: Constants.DEVICE_WIDTH,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        paddingLeft: 20,
        //backgroundColor: 'pink'
    },
    details_albumName: {
        fontSize: 28,
        color: '#FFF',
        textAlign: 'center',
        width: "100%"
        //fontWeight: 'bold',
    },
    details_artistName: {
        fontSize: 16,
        fontStyle: 'italic',
        color: '#FFF',
        textAlign: 'center',
        paddingBottom: 50,
        width: "100%"
    },
    details_annee: {
        fontSize: 16,
        fontStyle: 'italic',
        color: '#FFF',
        textAlign: 'center',
    },
    details_info: {
        paddingVertical: 18,
        paddingHorizontal: 18,
        //marginVertical: 2,
        backgroundColor: 'rgba(0,0,0,.9)'
    },
    details_info_label: {
        color: '#999',
        textAlign: 'center',
        fontSize: 19,
        //fontStyle: 'italic',
        paddingBottom: 10,
        //fontWeight: 'bold',
        //textDecorationLine: 'underline',
    },
    details_info_content: {
        color: '#FFF',
        fontStyle: 'italic',
        textAlign: 'center',
        fontSize: 15,
    }
})
