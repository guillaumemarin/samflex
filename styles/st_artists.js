import { StyleSheet } from 'react-native'
import * as Constants from '../constants/Constants'
import consts from "expo-constants";

export default StyleSheet.create({
    artist: {
        //backgroundColor: 'red',
        height: (Constants.DEVICE_WIDTH / 2) - 5,
        width: (Constants.DEVICE_WIDTH / 2) - 30,
        marginHorizontal: 10,
        borderRadius: 4,
        overflow: 'hidden',
        //borderWidth: 1,
        //borderColor: "#ddd",
        marginBottom: 15,
        //alignItems: "stretch"
    },
    image: {
        height: (Constants.DEVICE_WIDTH / 2) - 30,
        borderRadius: 4,
    },
    artistName: {
        paddingVertical: 7,
        color:  '#ccc'
    },
    scrollView: {
        maxHeight: Constants.DEVICE_HEIGHT - 24,
        paddingBottom: 90,
        //flexWrap: 'wrap',
        //flexDirection: 'row',
        //flex: 1,
        //backgroundColor: 'red'
    },
})
