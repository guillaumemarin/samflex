import * as React from 'react';

export const navigationDrawer = React.createRef();

export function navigate(name, params) {
    navigationDrawer.current?.navigate(name, params);
}
