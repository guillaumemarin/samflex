import React from "react";

import { StyleSheet, View, Text, ActivityIndicator } from "react-native";

import colors from '../constants/Colors'

class Loader extends React.Component {
    render() {
        return (
            <View style={styles.loader}>
                <Text style={styles.loaderText}>
                    {/*{this.props.title ? this.props.title : 'Veuillez patienter...'}*/}
                </Text>
                <ActivityIndicator size="large" color="#FFF"/>
            </View>
        )
    }
}

const styles = StyleSheet.create( {
    loader: {
        position: 'absolute',
        bottom: 0,
        top: 0,
        left: 0,
        right: 0,
        zIndex: 990,
        backgroundColor: "#00000099",
        width: "100%",
        height: "100%",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    loaderText: {
        color: colors.rouge,
        marginBottom: 10,
        fontSize: 20,
        width: "100%",
        textAlign: 'center'
    }
})

export default Loader;
