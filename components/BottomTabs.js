import React from "react";

import * as RootNavigation from '../components/RootNavigation.js';
import { View, StyleSheet, TouchableOpacity, Text, Image } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'

import colors from "../constants/Colors"


export default class BottomTabs extends React.Component{

    state = {
        currentScreen: 'Home',
        loggedIn: false,
    }

    constructor(props) {
        super(props);
    }

    navigate(screen){
        this.props.functions.hidePlayer()
        this.props.functions.setCurrentScreen(screen)

        RootNavigation.navigate( screen, {
            //dataGetToken: this.props.navigation.getParam( "dataGetToken" )
        })
    }

    setCurrentScreen = (name) => {
        this.setState({'currentScreen': name})
    }

    logout(){
        this.props.functions.logout()
    }

    setLoggedOut(){
        this.setState({loggedIn: false})
    }

    login(){
        this.setState({loggedIn: true})
    }

    showLogin(){
        this.props.functions.showLogin()
    }

    toggleParams = () => {
        this.props.functions.toggleParams()
    }


    render(){
        return (
            <View style={navStyle.tabsContainer}>

                <TouchableOpacity style={[navStyle.tab, this.state.currentScreen == 'Home' ? navStyle.tabActive : '' ]} onPress={() => { this.navigate( "Home" ) }}>
                    <MaterialCommunityIcons name="home-outline" size={24} color="white" />
                    {/*<Text style={navStyle.label}>Accueil</Text>*/}
                </TouchableOpacity>

                <TouchableOpacity style={[navStyle.tab, this.state.currentScreen == 'Artistes' ? navStyle.tabActive : '' ]} onPress={() => { this.navigate( "Artistes" ) }}>
                    <MaterialCommunityIcons name="artist-outline" size={24} color="white" />
                    {/*<Text style={navStyle.label}>Mes Artistes</Text>*/}
                </TouchableOpacity>

                <TouchableOpacity style={[navStyle.tab ]} onPress={() => { this.toggleParams() }}>
                    <MaterialCommunityIcons name="menu-open" size={24} color="white" />
                    {/*<Text style={navStyle.label}>Menu</Text>*/}
                </TouchableOpacity>
            </View>
        )
    }
}

const navStyle = StyleSheet.create({
    tabsContainer: {
        zIndex: 100,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#000',
        alignItems: 'stretch',
        height: 60,
        //borderTopColor: "#111",
        //borderTopWidth: 1,
    },
    tab: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: "relative",
        height: 60
    },
    tabActive: {
        backgroundColor: '#111'
    },
    label: {
        fontSize: 8,
        marginTop: 3,
        color: "#F5F5F5"
    },
    tabNotification: {
        position : 'absolute',
        top: 13,
        right: 13,
        height: 13,
        width: 13,
        borderRadius: 13,
        backgroundColor: colors.rouge,
        alignItems: 'center',
    },
    tabNotificationText: {
        color: "#FFF",
        fontSize: 7,
        fontWeight: 'bold',
        lineHeight: 12
    }
})
