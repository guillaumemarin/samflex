import React from "react";
import { Audio, Video } from "expo-av";

import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Dimensions, Slider, StyleSheet, Text, View, TouchableOpacity, Image, ScrollView } from "react-native";
import playerHelper from "../helpers/playerHelper";
import * as Constants from "../constants/Constants";
import { LinearGradient } from "expo-linear-gradient";

import * as ScreenOrientation from 'expo-screen-orientation';
import Api from "../api/Api";


class Icon {
    constructor(module, width, height) {
        this.module = module;
        this.width  = width;
        this.height = height;
    }
}

const ICON_PLAY_BUTTON = new Icon( "", 34, 51 )
const ICON_MUTED_BUTTON = new Icon( "", 67, 58 )

const LOOPING_TYPE_ALL = 0;
const LOOPING_TYPE_ONE = 1;

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
const BACKGROUND_COLOR = "#333";

const DISABLED_OPACITY = 0.5;
const FONT_SIZE = 14;
const LOADING_STRING = "...chargement...";
const BUFFERING_STRING = "...chargement...";
const VIDEO_CONTAINER_HEIGHT = (DEVICE_HEIGHT * 2.0) / 5.0 - FONT_SIZE * 2;

export default class PlayerAudio extends React.Component {
    constructor(props) {
        super(props);
        this.index = 0;
        this.isSeeking = false;
        this.queue_tracks = [];
        this.shouldPlayAtEndOfSeek = false;
        this.playbackInstance = null;
        this.statsInterval = null;
        this.state = {
            showVideo: false,
            playbackInstanceName: LOADING_STRING,
            loopingType: LOOPING_TYPE_ALL,
            muted: false,
            playbackInstancePosition: null,
            playbackInstanceDuration: null,
            shouldPlay: true,
            isPlaying: false,
            isBuffering: false,
            isLoading: true,
            fontLoaded: true,
            shouldCorrectPitch: true,
            volume: 1.0,
            rate: 1.0,
            poster: false,
            useNativeControls: false,
            fullscreen: false,
            throughEarpiece: false,
            show: false,
            playlist: null,
            videoWidth: DEVICE_WIDTH,
            videoHeight: VIDEO_CONTAINER_HEIGHT,
            videoUri: null,
            lastPlayed: 1,
            playlist_id: null,
            current_uri: null,
            statsTimer: 10
        }
    }

    componentDidMount() {
        Audio.setAudioModeAsync({
            allowsRecordingIOS: false,
            staysActiveInBackground: true,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
            playsInSilentModeIOS: true,
            shouldDuckAndroid: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            playThroughEarpieceAndroid: false
        })

        ScreenOrientation.addOrientationChangeListener(this._onOrientationChange)
    }

    _onOrientationChange = (event) => {
        if( event.orientationInfo.orientation == 4 || event.orientationInfo.orientation == 3 ){
            if (this.state.isPlaying && this.state.showVideo == true){
                console.log('this._video.presentFullscreenPlayer()')
                this._video.presentFullscreenPlayer()
            } else {
                if (this.state.showVideo == true){
                    console.log('this._video.dismissFullscreenPlayer()')
                    this._video.dismissFullscreenPlayer()
                }
            }
        } else {
            if (this.state.showVideo == true){
                console.log('this._video.dismissFullscreenPlayer()')
                this._video.dismissFullscreenPlayer()
            }
        }
    }

    async setPlaylist(NEWPLAYLIST, from){
        this.index = from
        console.log('Load new playlist')
        await this.setState({playlist: NEWPLAYLIST.tracks, playlist_id: NEWPLAYLIST.id})

        // if( NEWPLAYLIST.id != this.state.playlist_id || NEWPLAYLIST.id == 0 ){ // Chargement nouvelle playliste
        //
        //     console.log('Load new playlist')
        //     await this.setState({playlist: NEWPLAYLIST.tracks, playlist_id: NEWPLAYLIST.id})
        //
        // } else { // Pas de nouvelle playliste, on joue le morceau de l'index correspondant
        //
        //     console.log('Play track from actual playlist')
        //
        // }
        console.log('from: '+from)
        await this._loadNewPlaybackInstance(from)
        this.playTrack(from)
    }

    async addToCurrentPlaylist(playlist){
        console.log("addToCurrentPlaylist")
        if( ! this.state.playlist ){

            console.log('Set New Playlist')

            await this.setPlaylist(playlist, 0)

        } else {

            console.log('Add to playlist')

            let currentPlaylist = this.state.playlist
            if(playlist.tracks){
                playlist.tracks.map((track, i) => {
                    currentPlaylist.push(track)
                })
            }

            this.setState({playlist: currentPlaylist})

        }
    }

    _updateScreenForLoading(isLoading) {
        if(isLoading) {
            this.setState({
                isPlaying: false,
                playbackInstanceName: LOADING_STRING,
                playbackInstanceDuration: null,
                playbackInstancePosition: null,
                isLoading: true
            })
        } else {
            this.setState({
                playbackInstanceName: this.state.playlist[this.index].name,
                isLoading: false
            })
        }
    }

    _onPlaybackStatusUpdate = (status) => {
        if (status.isLoaded) {
            this.setState({
                playbackInstancePosition: status.positionMillis,
                playbackInstanceDuration: status.durationMillis,
                shouldPlay: status.shouldPlay,
                isPlaying: status.isPlaying,
                isBuffering: status.isBuffering,
                rate: status.rate,
                muted: status.isMuted,
                volume: status.volume,
                loopingType: status.isLooping ? LOOPING_TYPE_ONE : LOOPING_TYPE_ALL,
                shouldCorrectPitch: status.shouldCorrectPitch
            })
            if (status.didJustFinish && !status.isLooping) {
                if( this.state.playlist.length > 1 ){
                    this._advanceIndex(true)
                    this._updatePlaybackInstanceForIndex(true)
                }
            }

        } else {

            if (status.error) {
                console.log(`FATAL PLAYER ERROR: ${status.error}`)
            }

        }
    };

    _onLoadStart = () => {
        console.log(`LOAD START`);
    };

    _onLoad = status => {
        console.log(`LOADING : ${JSON.stringify(status)}`);
    };

    _onError = error => {
        console.log(`ERROR : ${error}`);
    };

    _advanceIndex(forward) {
        this.index = (this.index + (forward ? 1 : this.state.playlist.length - 1)) % this.state.playlist.length
    }

    async _updatePlaybackInstanceForIndex(playing) {
        console.log("_updatePlaybackInstanceForIndex")
        this._updateScreenForLoading(true);
        this._loadNewPlaybackInstance(playing);
    }

    _onPlayPausePressed = () => {
        if (this.playbackInstance != null) {
            if (this.state.isPlaying) {
                console.log('pause()')
                this.playbackInstance.pauseAsync();
            } else {
                console.log('play()')
                this.playbackInstance.playAsync();
            }
        }
    }


    _pauseAudio(){
        if (this.state.isPlaying) {
            console.log('pause()')
            this.playbackInstance.pauseAsync();
        }
    }

    _onForwardPressed = () => {
        if (this.playbackInstance != null) {
            this._advanceIndex(true);
            this._updatePlaybackInstanceForIndex(this.state.shouldPlay);
        }
    }

    _onBackPressed = () => {
        if (this.playbackInstance != null) {
            this._advanceIndex(false)
            this._updatePlaybackInstanceForIndex(this.state.shouldPlay)
        }
    }

    _onSeekSliderValueChange = value => {
        if (this.playbackInstance != null && !this.isSeeking) {
            this.isSeeking = true
            this.shouldPlayAtEndOfSeek = this.state.shouldPlay
            this.playbackInstance.pauseAsync()
        }
    }

    _onSeekSliderSlidingComplete = async value => {
        if (this.playbackInstance != null) {
            this.isSeeking = false
            const seekPosition = value * this.state.playbackInstanceDuration
            if (this.shouldPlayAtEndOfSeek) {
                this.playbackInstance.playFromPositionAsync(seekPosition)
            } else {
                this.playbackInstance.setPositionAsync(seekPosition);
            }
        }
    }

    _getSeekSliderPosition() {
        if (
            this.playbackInstance != null &&
            this.state.playbackInstancePosition != null &&
            this.state.playbackInstanceDuration != null
        ) {
            return (
                this.state.playbackInstancePosition /
                this.state.playbackInstanceDuration
            )
        }
        return 0
    }

    _getMMSSFromMillis(millis) {
        const totalSeconds = millis / 1000
        const seconds = Math.floor(totalSeconds % 60)
        const minutes = Math.floor(totalSeconds / 60)

        const padWithZero = number => {
            const string = number.toString()
            if (number < 10) {
                return "0" + string
            }
            return string
        };
        return padWithZero(minutes) + ":" + padWithZero(seconds)
    }

    _getTimestamp() {
        if (
            this.playbackInstance != null &&
            this.state.playbackInstancePosition != null &&
            this.state.playbackInstanceDuration != null
        ) {
            return `${this._getMMSSFromMillis(
                this.state.playbackInstancePosition
            )} / ${this._getMMSSFromMillis(this.state.playbackInstanceDuration)}`
        }
        return ""
    }

    _onPosterPressed = () => {
        this.setState({ poster: !this.state.poster })
    }

    togglePlayerVisibility = () => {
        let visibility = this.state.show == true ? false : true
        this.setState({'show': visibility})
        this.props.showPlayer()
    }

    hidePlayer = () => {
        this.setState({'show': false})
    }

    showPlayer = () => {
        this.setState({'show': true})
    }

    playTrack = (trackNumber) => {
        if( this.index == trackNumber ){
            console.log('onPlayPausePressed()')
            this._onPlayPausePressed()
        } else {
            if (this.playbackInstance != null) {
                this.index = trackNumber
                this.setState({shouldPlay: trackNumber})
                this._updatePlaybackInstanceForIndex(true)
            }
        }
    }

    _mountAudio = component => {
        this._video = component;
        //this._loadNewPlaybackInstance(false);
    };

    async playVideo(video){

        console.log('playVideo()')
        let videoList = playerHelper.builVideoItem(video)

        await this.setState({playlist_id: 0, playlist: videoList.tracks, showVideo: true})

        this.index = 0
        await this._loadNewPlaybackInstance(0)

        this.playTrack(0)
    }

    /************************************************************************************************/
    /* STATS TIMER */
    /************************************************************************************************/

    stopStatsTimer = () => {
        clearInterval(this.statsInterval)
        this.statsInterval = null
        this.setState({statsTimer: 10})
    }

    startStatsTimer = async () => {
        if( ! this.statsInterval ){
            this.statsInterval = setInterval(async () => {
                this.setState({statsTimer: this.state.statsTimer - 1})

                console.log(this.state.statsTimer)

                if(this.state.statsTimer <= 0){
                    this.stopStatsTimer()

                    console.log('Add view stat')

                    let media = this.state.playlist[this.index];

                    let type = media.isVideo == true ? 'video' : 'audio'
                    let incrementViews = await Api.incrementMediaView(type, media.id, 'this.state.dataGetToken')
                    //let incrementViews = await Api.incrementMediaView(type, media.id, this.state.dataGetToken)

                    console.log(incrementViews)
                }
            }, 1000)
        }
    }

    /************************************************************************************************/
    /* PLAY TRACK */
    /************************************************************************************************/

    async _loadNewPlaybackInstance(playing) {
        console.log('_loadNewPlaybackInstance()' + playing)

        this.stopStatsTimer()

        let lastPlayed = this.state.lastPlayed + 1;
        this.setState({lastPlayed: lastPlayed})

        if (this.playbackInstance != null) {

            if(this.state.showVideo == true){
                console.log('dismissFullscreenPlayer()')
                if(this.state.fullscreen == true){
                    let exitFS = await this._video.dismissFullscreenPlayer()
                    this.props._setOrientationPortrait(false)
                    this.setState({fullscreen: false})
                }
            }

            console.log("unloadTrack")
            await this.playbackInstance.unloadAsync()
            this.playbackInstance.setOnPlaybackStatusUpdate(null)
            this.playbackInstance = null
        }

        let newUri = this.state.playlist[this.index].uri;
        const source = { uri: newUri }

        await this.setState({current_uri: newUri})

        this.props.setCurrentTrack(newUri)

        const initialStatus = {
            shouldPlay: playing,
            rate: this.state.rate,
            shouldCorrectPitch: this.state.shouldCorrectPitch,
            volume: this.state.volume,
            isMuted: this.state.muted,
            isLooping: this.state.loopingType === LOOPING_TYPE_ONE,
            // UNCOMMENT THIS TO TEST THE OLD androidImplementation:
            // androidImplementation: 'MediaPlayer',
        }

        if (this.state.playlist[this.index].isVideo) {

            this.setState({showVideo: true})
            await this._video.loadAsync(source, initialStatus)

            this.playbackInstance = this._video
            const status = await this._video.getStatusAsync()

        } else {

            this.setState({showVideo: false})

            let load_success = false;
            try{
                const { sound, status } = await Audio.Sound.createAsync(
                    source,
                    initialStatus,
                    this._onPlaybackStatusUpdate,
                )

                let loadedTracks = this.queue_tracks
                loadedTracks.push({"uri": source.uri, "instance": sound})

                // Fix bug : On décharge les instances si plusieurs lancées avant le buffering
                if(loadedTracks.length > 1){
                    console.log("loadedTracks.length : " + loadedTracks.length)
                    loadedTracks.map( async (track, i) => {
                        console.log('uri : ' + track.uri)
                        if( this.state.current_uri != track.uri ){

                            await track.instance.unloadAsync()
                            console.log("delete loadedTracks " + i)
                            delete loadedTracks[i]

                        }
                    })
                }

                this.startStatsTimer()
                this.playbackInstance = sound

            } catch(error){
                alert('Problème de lecture sur ce titre. Merci de réessayer plus tard :(')
            }

        }

        this._updateScreenForLoading(false)
    }

    _onReadyForDisplay = event => {
        const widestHeight = (DEVICE_WIDTH * event.naturalSize.height) / event.naturalSize.width;
        if (widestHeight > VIDEO_CONTAINER_HEIGHT) {
            this.setState({
                videoWidth:
                    (VIDEO_CONTAINER_HEIGHT * event.naturalSize.width) /
                    event.naturalSize.height,
                    videoHeight: VIDEO_CONTAINER_HEIGHT
            });
        } else {
            this.setState({
                videoWidth: DEVICE_WIDTH,
                videoHeight:
                    (DEVICE_WIDTH * event.naturalSize.height) / event.naturalSize.width
            });
        }
    };

    _onFullscreenUpdate = event => {
        if(event.fullscreenUpdate == 2){
            this.props._setOrientationPortrait(false)
        }
    };

    onpenActions = () => {
        alert('Todo :/')
    }

    _onFullscreenPressed = () => {
        try {
            this._video.presentFullscreenPlayer();
            this.setState({fullscreen: true})
            this.props._setOrientationPortrait(true)
        } catch (error) {
            console.log(error.toString());
        }
    };

    render() {
        return this.state.playlist == null ? (
            <View style={styles.emptyContainer} />
        ) : (
            <View style={[styles.container, this.state.show ? styles.fullscreen : styles.minimized]}>

                <View style={styles.playerHeader}>
                    {
                        this.state.show == false ? (
                            <TouchableOpacity style={styles.playerHeaderLeft} onPress={this._onPlayPausePressed} disabled={this.state.isLoading}>
                                {
                                    this.state.isPlaying ? (
                                        <MaterialCommunityIcons name="pause" size={25} color="#FFF" />
                                    ) : (
                                        <MaterialCommunityIcons name="play" size={25} color="#FFF" />
                                    )
                                }
                            </TouchableOpacity>
                        ) : null
                    }
                    <TouchableOpacity style={[styles.playerHeaderRight, this.state.show ? styles.playerHeaderRightFull : '' ]}
                                      onPress={() => this.togglePlayerVisibility()}
                    >
                        <Text style={[styles.playerHeaderTitle]}>
                            {this.state.show == true ? '' : this.state.playbackInstanceName}
                        </Text>
                        {
                            this.state.show == true ? (
                                <MaterialCommunityIcons name="chevron-down" size={27} color="#FFF" />
                            ) : (
                                <MaterialCommunityIcons name="chevron-up" size={27} color="#FFF" />
                            )
                        }
                    </TouchableOpacity>
                </View>

                <Video ref={this._mountAudio}
                       onPlaybackStatusUpdate={this._onPlaybackStatusUpdate}
                       onLoadStart={this._onLoadStart}
                       onLoad={this._onLoad}
                       onError={this._onError}
                       style={[
                           styles.video,
                           {
                               opacity: this.state.showVideo ? 1.0 : 0.0,
                               width: this.state.videoWidth,
                               height: this.state.showVideo ? this.state.videoHeight : 0,
                               marginTop: this.state.showVideo ? 0 : 0
                           }
                       ]}
                       onFullscreenUpdate={this._onFullscreenUpdate}
                       resizeMode={Video.RESIZE_MODE_CONTAIN}
                       onReadyForDisplay={this._onReadyForDisplay}
                       useNativeControls={this.state.useNativeControls}
                />

                {
                    /************************************************************************************************/
                    /* PLAYLIST */
                    /************************************************************************************************/
                }

                <ScrollView style={styles.playlistScrollView} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false }>

                    {
                        this.state.showVideo == false ? (
                            <LinearGradient colors={['#111', '#000']} style={styles.playerInfosHeader}>

                                <Image style={styles.headerImage} source={{ uri: this.state.playlist[this.index].cover }} />
                                <View style={styles.headerInfos}>
                                    <Text style={styles.albumName}>
                                        { this.state.playlist[this.index].name }
                                    </Text>
                                    <Text style={styles.artistName}>
                                        { this.state.playlist[this.index].artistname }
                                    </Text>
                                </View>

                            </LinearGradient>
                        ) : null
                    }

                    {
                        this.state.playlist != null ? (
                            this.state.playlist.map( ( track, i ) => {
                                return(
                                    <View key={i} style={styles.listItem}>
                                        <TouchableOpacity onPress={() => {
                                            if( this.index != i ){
                                                this.playTrack(i)
                                            }
                                        }} style={{flexDirection: 'row', alignItems: 'center'}}>
                                            {
                                                this.index == i ? (
                                                    this.state.isPlaying ? (
                                                        <View style={{ width: 40 }}>
                                                            <Image style={{ height: 24, width: 24, opacity: .8, marginLeft: -1 }}
                                                                   source={require('../assets/images/equi.gif') }
                                                            />
                                                        </View>
                                                    ) : (
                                                        <View style={{ width: 40 }}>
                                                            <MaterialCommunityIcons name="play" size={22} style={{marginRight: 2, marginLeft: -2}} color="white" />
                                                        </View>
                                                    )
                                                ) : (
                                                    <View style={{ width: 40 }}>
                                                        <Image style={styles.listItemCover} source={{ uri: track.cover }} />
                                                    </View>
                                                )
                                            }
                                            <View style={{ width: DEVICE_WIDTH - 30 }}>
                                                <Text style={[styles.text, { lineHeight: 24, marginLeft: 5 }]}>{track.name}</Text>
                                                <Text style={{ lineHeight: 18, marginLeft: 7, fontSize: 11, fontStyle: 'italic', color: "#999" }}>
                                                    {track.artistname}
                                                </Text>
                                            </View>

                                        </TouchableOpacity>
                                    </View>
                                )
                            })
                        ) : null
                    }
                </ScrollView>

                <View style={{ flex: 1, width: "100%" }}>

                    {
                        /************************************************************************************************/
                        /* CONTROLS */
                        /************************************************************************************************/
                    }
                    <View style={styles.controlsContainer}>

                        {
                            /************************************************************************************************/
                            /* SLIDER */
                            /************************************************************************************************/
                        }
                        <Slider
                            style={styles.playbackSlider}
                            value={this._getSeekSliderPosition()}
                            onValueChange={this._onSeekSliderValueChange}
                            onSlidingComplete={this._onSeekSliderSlidingComplete}
                            disabled={this.state.isLoading}
                            minimumTrackTintColor="#FFF"
                            maximumTrackTintColor="#FFF"
                            thumbTintColor="#FFF"
                        />

                        <View style={[styles.playbackContainer, { opacity: this.state.isLoading ? DISABLED_OPACITY : 1.0 }]}>
                            <View style={styles.timestampRow}>
                                <Text style={[styles.text, styles.buffering]}>
                                    {this.state.isBuffering ? BUFFERING_STRING : ""}
                                </Text>
                                <Text style={[styles.text, styles.timestamp]}>
                                    {this._getTimestamp()}
                                </Text>
                            </View>
                        </View>

                        <View style={[styles.buttonsContainerBase, { opacity: this.state.isLoading ? DISABLED_OPACITY : 1.0 }]}>

                            <TouchableOpacity style={styles.controlsBtn} onPress={this._onBackPressed} disabled={this.state.isLoading}>
                                <MaterialCommunityIcons name="skip-backward" size={24} color="#333" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.controlsBtn} onPress={this._onPlayPausePressed} disabled={this.state.isLoading}>
                                {
                                    this.state.isPlaying ? (
                                        <MaterialCommunityIcons name="pause" size={24} color="#333" />
                                    ) : (
                                        <MaterialCommunityIcons name="play" size={24} color="#333" />
                                    )
                                }
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.controlsBtn} onPress={() => {this._onForwardPressed() }} disabled={this.state.isLoading}>
                                <MaterialCommunityIcons name="skip-forward" size={24} color="#333" />
                            </TouchableOpacity>
                            {
                                this.state.showVideo == true ? (
                                    <TouchableOpacity style={styles.controlsBtn} onPress={ () => {this._onFullscreenPressed()} } disabled={this.state.isLoading}>
                                        <MaterialCommunityIcons name="fullscreen" size={24} color="#333" />
                                    </TouchableOpacity>
                                ) : null
                            }
                        </View>
                    </View>
                </View>

            </View>
        );
    }
}

const coverHeight = 80;

const styles = StyleSheet.create({
    emptyContainer: {
        alignSelf: "stretch",
        backgroundColor: BACKGROUND_COLOR
    },
    container: {
        flex: 1,
        flexDirection: "column",
        height: DEVICE_HEIGHT,
        width: DEVICE_WIDTH,
        backgroundColor: "#050303",
        //backgroundColor: 'rgba(10, 10, 10, .99)',
    },
    playlistScrollView: {
        height: DEVICE_HEIGHT - 250,
        maxHeight: DEVICE_HEIGHT - 250,
    },
    playerHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        alignContent: 'center',
        width: "100%",
        backgroundColor: '#111',
        paddingHorizontal: 10,
    },
    playerInfosHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "100%",
        paddingHorizontal: 10,
        paddingBottom: 15
    },
    headerImage: {
        height: coverHeight,
        width: coverHeight,
        maxWidth: Constants.DEVICE_WIDTH / 2,
        maxHeight: Constants.DEVICE_WIDTH / 2,
        //borderRadius: 4
    },
    headerInfos: {
        height: Constants.DEVICE_WIDTH / 2,
        width: Constants.DEVICE_WIDTH - coverHeight - 30,
        maxHeight: coverHeight,
        //maxWidth: '50%',
        //paddingBottom: CoverMargin,
        paddingHorizontal: 15,
    },
    albumName: {
        fontSize: 17,
        fontWeight: 'bold',
        color: '#fff',
    },
    artistName: {
        fontSize: 12,
        fontStyle: 'italic',
        lineHeight: 16,
        color: '#fff',
    },
    playerHeaderRight: {
        paddingVertical: 15,
        paddingHorizontal: 20,
        width: DEVICE_WIDTH - 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    playerHeaderRightFull: {
        width: DEVICE_WIDTH
    },
    playerHeaderLeft: {
        width: 60,
        //paddingVertical: 20,
        alignItems: 'center',
    },
    playerHeaderTitle: {
        color: '#FFF',
        lineHeight: 23,
        fontWeight: 'bold'
    },
    minimized: {
        //bottom: 80 - DEVICE_HEIGHT
        paddingTop: 0
    },
    fullscreen: {
        paddingTop: 0
    },
    listItem: {
        flexDirection: 'row',
        paddingHorizontal: 12,
        paddingVertical: 15,
        borderTopWidth: 1,
        borderTopColor: '#111',
        alignItems: "center",
    },
    listItemCover: {
        width: 30,
        height: 30,
        borderRadius: 4,
    },
    wrapper: {},
    nameContainer: {
        //height: FONT_SIZE
    },
    space: {
        height: FONT_SIZE
    },
    videoContainer: {
        height: VIDEO_CONTAINER_HEIGHT,
    },
    video: {
        maxWidth: DEVICE_WIDTH,
        //backgroundColor: 'red',
        //marginTop: -10
    },
    playbackContainer: {
        flex: 1,
        flexDirection: "column",
        //justifyContent: "space-between",
        alignItems: "center",
        alignSelf: "stretch",
    },
    playbackSlider: {
        alignSelf: "stretch"
    },
    timestampRow: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        alignSelf: "stretch",
        minHeight: FONT_SIZE
    },
    text: {
        fontSize: FONT_SIZE,
        minHeight: FONT_SIZE,
        color: '#FFF'
    },
    buffering: {
        textAlign: "left",
        paddingLeft: 20,
        fontSize: 10,
        marginTop: -18,
        color: '#fff'
    },
    timestamp: {
        textAlign: "right",
        paddingRight: 20,
        fontSize: 10,
        marginTop: -18,
        color: '#fff'
    },
    button: {
        backgroundColor: BACKGROUND_COLOR
    },
    controlsContainer: {
        flexDirection: "column",
        height: 120,
        backgroundColor: 'rgba(0, 0, 0, .9)',
        //alignItems: "center",
        justifyContent: "center",
        width: "100%",
        position: "absolute",
        bottom: 60,
        left: 0,
        paddingVertical: 23,
    },
    controlsBtn: {
        marginLeft: 10,
        backgroundColor: '#FFF',
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        width: 40,
        borderRadius: 30
    },
    buttonsContainerBase: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
    },
    buttonsContainerTopRow: {
        maxHeight: ICON_PLAY_BUTTON.height,
        minWidth: DEVICE_WIDTH / 2.0,
        maxWidth: DEVICE_WIDTH / 2.0
    },
    buttonsContainerMiddleRow: {
        maxHeight: ICON_MUTED_BUTTON.height,
        alignSelf: "stretch",
        paddingRight: 20
    },
    volumeContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        minWidth: DEVICE_WIDTH / 2.0,
        maxWidth: DEVICE_WIDTH / 2.0
    },
    volumeSlider: {
        width: DEVICE_WIDTH / 2.0 - ICON_MUTED_BUTTON.width
    },
    rateSlider: {
        width: DEVICE_WIDTH / 2.0
    },
    buttonsContainerTextRow: {
        maxHeight: FONT_SIZE,
        alignItems: "center",
        paddingRight: 20,
        paddingLeft: 20,
        minWidth: DEVICE_WIDTH,
        maxWidth: DEVICE_WIDTH
    }
});
