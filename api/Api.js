import axios from "axios"

import * as Constants from '../constants/Constants'
import * as SecureStore from 'expo-secure-store'

class Api{

    // Urls API
    static api_base_url = Constants.API_BASE_URL;
    static api_url      = Constants.API_URL;

    /************************************************************************************************/
    /* AUTH */
    /************************************************************************************************/

    static async get_local_token(){
        let localToken = await SecureStore.getItemAsync('api_token');

        if( localToken ){
            return JSON.parse(localToken);
        }

        return null;
    }

    static async check_login(){
        let localToken = await SecureStore.getItemAsync('api_token');

        if( localToken ){
            let token_object  = JSON.parse(localToken)
            let alive = await this.api_alive(token_object)
            return alive
        }

        return null;
    }

    static async api_alive(localToken){
        console.log("api_alive()");

        try {
            let alive = await axios.get(
                this.api_url + "alive",
                {
                    headers: {
                        Authorization: "Bearer " + localToken.access_token
                    }
                }
            );

            console.log("Retour api = " + alive.data);

            return 1;

        } catch (err) {

            console.log("api_alive error : ", err);
            return {error: "Impossible de communiquer avec l'API !"};

        }
    }

    static async get_demo_token(){

        try {
            let res = await axios.post(
                this.api_base_url + "oauth/token",
                {
                    grant_type: "password",
                    username: apiConfig.demoEmail,
                    password: apiConfig.demoPassword,
                    client_secret: "cq3rRteGV1E63B52DENoktHuKSMW3HYBExsM2kdD",
                    client_id: "4"
                }
            )

            let data        = res.data
            let date        = new Date()
            data.expires_at = date.getTime() + res.data.expires_in

            return {data: data}

        } catch (err) {

            console.log("getToken error : ", err)
            return {error: "Une erreur est survenue, veuillez recommencer"}

        }
    }

    static async get_token(username, password){

        try {
            let res = await axios.post(
                this.api_base_url + "oauth/token",
                {
                    grant_type: "password",
                    username: username,
                    password: password,
                    client_secret: "1MseAbTSwFJlcjIfDxTaNnsHNJ1mZ0ypLyfuMrS5",
                    client_id: "920fe5b0-cf88-40e6-b553-37578c2fac66"
                }
            )

            let data        = res.data
            let date        = new Date()
            data.expires_at = date.getTime() + res.data.expires_in

            await SecureStore.setItemAsync( "api_token", JSON.stringify( data ) )

            return {data: data}

        } catch (err) {

            console.log("getToken error : ", err)
            return {error: "Une erreur est survenue, veuillez recommencer"}

        }
    }

    /************************************************************************************************/
    /* ARTISTS */
    /************************************************************************************************/

    static async get_artist(dataToken, idArtist){

        try {
            let artist = await axios.get(
                this.api_url + "artist/" + idArtist,
                {
                    headers: {
                        Authorization: "Bearer " + dataToken.access_token
                    }
                }
            );

            return artist.data;

        } catch (err) {

            console.log("get_artist error : ", err);
            return {error: "Impossible de récupérer l'artiste !"};

        }
    }

    static async get_artist_posts(dataToken, idArtist){

        try {
            let artist = await axios.get(
                this.api_url + "artist/" + idArtist + "/posts",
                {
                    headers: {
                        Authorization: "Bearer " + dataToken.access_token
                    }
                }
            );

            return artist.data;

        } catch (err) {

            console.log("get_artist error : ", err);
            return {error: "Impossible de récupérer l'artiste !"};

        }
    }

    static async get_artists(dataToken){
        try {
            let getArtists = await axios.get(this.api_url + "artists", {
                headers: {
                    Authorization: "Bearer " + dataToken.access_token
                }
            });
            return getArtists.data;
        } catch (err) {
            console.log("erreur", err);
            return {error: "Impossible de récupérer la liste des artistes !"}
        }
    }

    /************************************************************************************************/
    /* ALBUMS */
    /************************************************************************************************/

    static async get_album(dataToken, id){
        try {
            let album = await axios.get(
                this.api_url + "album/" + id,
                {
                    headers: {
                        Authorization: "Bearer " + dataToken.access_token
                    }
                }
            );

            return album.data;

        } catch (err) {

            console.log("get_artist error : ", err);
            return {error: "Impossible de récupérer l'artiste !"};

        }
    }

    /************************************************************************************************/
    /* SINGLES */
    /************************************************************************************************/

    static async get_single(dataToken, id){
        try {
            let single = await axios.get(
                this.api_url + "single/" + id,
                {
                    headers: {
                        Authorization: "Bearer " + dataToken.access_token
                    }
                }
            );

            return single.data;

        } catch (err) {

            console.log("get_single error : ", err);
            return {error: "Impossible de récupérer le single !"};

        }
    }

    /************************************************************************************************/
    /* NOTIFICATIONS */
    /************************************************************************************************/

    static async notifications(dataToken){
        try {
            let getNotifications = await axios.get(this.api_url + "notifications", {
                headers: {
                    Authorization: "Bearer " + dataToken.access_token
                }
            });

            return getNotifications.data

        } catch (err) {
            console.log("erreur", err);
            return null
        }
    }

    static async incrementMediaView(type, id, dataToken){

        try {
            let setRead = await axios.post(this.api_url + "stats/streams",
                {
                    id: id,
                    type: type
                },
                {
                    headers: {
                        Authorization: "Bearer " + dataToken.access_token
                    }
                }
            )

            return setRead.data;

        } catch (err) {

            console.log("erreur", err);

            return null

        }
    }

    static async setNotificationAsRead(notification_id, dataToken){
        try {
            let setRead = await axios.post(this.api_url + "notifications/read",
                {id: notification_id},
                {
                    headers: {
                        Authorization: "Bearer " + dataToken.access_token
                    }
                }
            );
            return setRead.data;
        } catch (err) {
            console.log("erreur", err);
            return null
        }
    }

    /************************************************************************************************/
    /* USER */
    /************************************************************************************************/

    static async me(dataToken){
        try {
            let getUser = await axios.get(this.api_url + "user", {
                headers: {
                    Authorization: "Bearer " + dataToken.access_token
                }
            });

            return getUser.data;
        } catch (err) {
            console.log("erreur", err);
            return null
        }
    }

    static async register(nom, prenom, email, password, password_confirm){
        try {
            let inscription = await axios.post(this.api_url + "inscription",
                {
                    name: nom + ' ' + prenom,
                    email: email,
                    password: password,
                    password_confirmation: password_confirm,
                }
            );
            return inscription.data;
        } catch (err) {
            console.log("erreur", err);
            return null
        }
    }

    static async subscribe(dataToken, amount, artist_id){

        try {

            let subscription = await axios.post(this.api_url + "subsbribe", {
                artist_id: artist_id,
                amount: amount,
            }, {
                headers: {
                    Authorization: "Bearer " + dataToken.access_token
                }
            });

            return subscription.data;

        } catch (err) {

            console.log("erreur", err);
            return null

        }
    }

}

export default Api;
